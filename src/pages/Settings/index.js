import React, { useState } from 'react';
import { Card, TextField } from '@material-ui/core';
import cls from './settings.module.scss';
import ButtonStyle from '../../components/ButtonStyle';
import { useSelector, useDispatch } from 'react-redux';
import authentication from '../../redux/actions/authentication';
import { useFormik } from 'formik';
import InputForm from '../../components/form/InputForm';
import validate from './validate';
import { useHistory } from 'react-router-dom';

const Settings = () => {
    const history = useHistory();
    const [changePassword, setChangePassword] = useState(false);
    const { orgShortName, accBalanceDt, unp } = useSelector(state => state.authentication.user);
    const dispatch = useDispatch();
    const { values, handleSubmit, handleChange, handleBlur, errors, touched } = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        validate: (values) => validate(values),
        onSubmit: ({ firstPassword }) => {
            dispatch(authentication.resetPwd(firstPassword));
        },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    return (
        <Card className={cls.settings_container_wrapper}>
            <h1>Настройки аккаунта</h1>
            <div className={cls.settings_container_info_block_wrapper}>
                <div className={cls.settings_container_info_block}>
                    <div>
                        Дата регистрации в Системе: {accBalanceDt}
                    </div>
                    <div>
                        Последнее изменение: {accBalanceDt}
                    </div>
                </div>
            </div>
            <div className={cls.settings_container_contacts_block_wrapper}>
                <div className={cls.settings_text_block}>
                    УНП: {unp}
                </div>
                <div className={cls.settings_text_block}>
                    Полное наименование: {orgShortName}
                </div>
                <div>
                    <div className={cls.settings_button_block}>
                        <span>Контактный телефон:</span>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            size="small"
                            label='телефон'
                            value='202-03-27'
                        />
                    </div>
                    <div className={cls.settings_button_block}>
                        <span>Контактный e-mail:</span>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            size="small"
                            label='e-mail'
                            value='zpos@mail.ru'
                        />
                    </div>
                </div>
            </div>
            <form onSubmit={handleSubmit}>
                <div className={cls.change_password}>
                    {!changePassword ?
                        <ButtonStyle onClick={() => setChangePassword(true)}>изменить пароль</ButtonStyle> :

                        <div className={cls.change_password_button_block}>
                            <div className={cls.settings_button_block}>
                                <span>Введите новый пароль:</span>
                                <InputForm
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    values={values}
                                    name="firstPassword"
                                    label="пароль"
                                    touched={touched}
                                    errors={errors}
                                    type="password"
                                />
                            </div>
                            <div className={cls.settings_button_block}>
                                <span>Повторить пороль:</span>
                                <InputForm
                                    handleChange={handleChange}
                                    handleBlur={handleBlur}
                                    values={values}
                                    name="secondPassword"
                                    label="Подтвердить пароль"
                                    touched={touched}
                                    errors={errors}
                                    type="password"
                                />
                            </div>
                        </div>
                    }
                </div>
                <div className={cls.button_block_management}>
                    <ButtonStyle styleVariable='outlined' onClick={() => {
                        history.go(-1)
                    }}>отменить</ButtonStyle>
                    <ButtonStyle type='submit'>сохранить</ButtonStyle>
                </div>
            </form>
        </Card>
    )
}

export default Settings;