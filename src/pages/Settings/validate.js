const validate = (values, resetPassword /* only available when using withFormik */) => {
    const errors = {};

      if (!values.firstPassword) {
        errors.firstPassword = 'Введите пароль';
      }
      if (!values.secondPassword) {
        errors.secondPassword = 'Введите пароль';
      }
      if (values.secondPassword !== values.firstPassword && values.secondPassword && values.firstPassword) {
        errors.secondPassword = 'Не совпадает';
        errors.firstPassword = 'Не совпадает';
      }
  
    return errors;
  };
  
  export default validate;