/* eslint-disable no-octal-escape */
/* eslint-disable react/jsx-no-target-blank */
import React, { useState } from 'react';
import cls from './faq.module.scss'
import { ExpansionPanel, ExpansionPanelSummary, Typography, ExpansionPanelDetails } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import HelmetComponent from '../../components/HelmetComponent';
import { useTranslation } from 'react-i18next';
import QR from '../../img/qr.jpg';

const FAQ = () => {
    const { t } = useTranslation();
    const FaqArray = [
        {
            title: t('pages.faq.question1.title'),
            description: (<>
                <span className={cls.faq_text_block}>
                    {t('pages.faq.question1.description1')}
                </span>
                <span className={cls.faq_text_block}>
                    {t('pages.faq.question1.description2')}<br />
                    {t('pages.faq.question1.description3')}
                </span>
                <span className={cls.faq_text_block}>
                    {t('pages.faq.question1.description4')}
                </span>
                <span className={cls.faq_text_block}>
                    {t('pages.faq.question1.description5')}
                </span>
                <span className={cls.faq_text_block}>
                    {t('pages.faq.question1.description6')}
                </span>
            </>)
        },
        {
            title: t('pages.faq.question2.title'),
            description: (<>
                <span className={cls.faq_text_block}>
                    {t('pages.faq.question2.description1')}
                    <a href="https://web.cloudshop.ru/anonymous/login/" target="_blank" rel="noopener noreferrer">{t('pages.faq.question2.description2')}</a>
                </span>
            </>)
        },
        {
            title: t('pages.faq.question3.title'),
            description: (<>
                <span className={cls.faq_text_block}>{t('pages.faq.question3.description1')}
                    <a href="https://zpos.by/Rukovodstvo_polzovatelja_zPOS_Kassa.pdf" target="_blank" rel="noopener noreferrer">{t('pages.faq.question3.description2')}</a> </span>
            </>)
        },
        {
            title: t('pages.faq.question4.title'),
            description: (<>
                <span className={cls.faq_text_block}>{t('pages.faq.question4.description1')} </span>
            </>)
        },
        {
            title: t('pages.faq.question5.title'),
            description: (<>
                <span className={cls.faq_text_block}>{t('pages.faq.question5.description1')}
                    <a href="https://zpos.by/Rukovodstvo_polzovatelja_zPOS_Kassa.pdf" target="_blank" rel="noopener noreferrer">{t('pages.faq.question5.description2')}</a> </span>
            </>)
        },
        {
            title: t('pages.faq.question6.title'),
            description: (<>
                <span className={cls.faq_text_block}>{t('pages.faq.question6.description1')}</span>
                <span className={cls.faq_text_block}>
                    <div className={cls.faq_text_table}>-{' '}{t('pages.faq.question6.description2')}</div>
                    <div className={cls.faq_text_table}>-{' '}{t('pages.faq.question6.description3')}</div>
                    <div className={cls.faq_text_table}>-{' '}{t('pages.faq.question6.description4')}</div>
                    <div className={cls.faq_text_table}>-{' '}{t('pages.faq.question6.description5')}</div>
                    <div className={cls.faq_text_table}>-{' '}{t('pages.faq.question6.description6')}</div>
                </span>
                <span className={cls.faq_text_block}>{t('pages.faq.question6.description7')}</span>
            </>)
        },
        {
            title: t('pages.faq.question7.title'),
            description: (<>
                <span className={cls.faq_text_block}>{t('pages.faq.question7.description1')}</span>
            </>)
        },
        {
            title: t('pages.faq.question8.title'),
            description: (<>
                <span className={cls.faq_text_block}>{t('pages.faq.question8.description1')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question8.description2')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question8.description3')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question8.description4')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question8.description5')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question8.description6')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question8.description7')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question8.description8')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question8.description9')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question8.description10')}</span>
            </>)
        },
        {
            title: t('pages.faq.question9.title'),
            description: (<>
                <span className={cls.faq_text_block}>{t('pages.faq.question9.description1')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question9.description2')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question9.description3')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question9.description4')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question9.description5')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question9.description6')} <a href="http://e-pay.by/" target="_blank" rel="e-pay.by">e-pay.by</a>.</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question9.description7')}</span>
                <span className={cls.faq_text_block}><img src={QR} alt='QR' /></span>
            </>)
        },
        {
            title: t('pages.faq.question10.title'),
            description: (<>
                <span className={cls.faq_text_block}>{t('pages.faq.question10.description1')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question10.description2')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question10.description3')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question10.description4')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question10.description5')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question10.description6')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question10.description7')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question10.description8')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question10.description9')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question10.description10')}</span>
            </>)
        },
        {
            title: t('pages.faq.question11.title'),
            description: (<>
                <span className={cls.faq_text_block}>{t('pages.faq.question11.description1')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question11.description2')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question11.description3')}</span>
                <span className={cls.faq_text_block}>{t('pages.faq.question11.description4')}</span>
            </>)
        },
    ]
    const [expanded, setExpanded] = useState(false);
    const handleChange = panel => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };
    return (
        <div className={cls.faq_container}>
            <HelmetComponent titlePage={t('pages.faq.helmet.titlePage')} description={t('pages.faq.helmet.description')} title={t('pages.faq.helmet.title')} />
            <h1><span className={cls.faq_line} />{t('pages.faq.title')}<span className={cls.faq_line} /></h1>
            {FaqArray.map(({ title, description }, index) => (
                <ExpansionPanel expanded={expanded === index} key={index} onChange={handleChange(index)}>
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1bh-content"
                        id="panel1bh-header"
                    >
                        <Typography className={cls.title_style}>{title}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className={cls.faq_detalis}>
                        {description}
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            ))}
        </div>
    )
};

export default FAQ;