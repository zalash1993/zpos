import React from 'react';
import PropTypes from 'prop-types';
import {
    TableCell,
    TableHead,
    TableRow,
    TableSortLabel,
    Select,
    MenuItem
} from '@material-ui/core';
import cls from './style.module.scss';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import CheckboxStyle from '../../../../../../components/CheckboxStyle';

const EnhancedTableHead = ({ onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort, headCells }) => {
    const createSortHandler = property => event => {
        onRequestSort(event, property);
    };
    return (
        <TableHead>
            <TableRow className={cls.head_table_wrapper}>
                <TableCell className={cls.head_table_checkbox}>
                    <CheckboxStyle
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                    />
                    <Select
                        value={orderBy}
                        className={cls.select_visable}
                        onChange={(el) => {
                            createSortHandler(el.target.value)()
                        }}
                        style={{ width: '100%' }}
                    >
                        {headCells.map(headCell => (
                            <MenuItem key={headCell} onClick={createSortHandler(headCell.id)} value={headCell.id}>
                                {headCell.label}
                                {orderBy === headCell.id &&
                                    <>{order === 'asc' ?
                                        <ArrowUpwardIcon color="action" fontSize="small" /> :
                                        <ArrowUpwardIcon style={{ transform: 'rotate(180deg)' }} color="action" fontSize="small" />}
                                    </>}
                            </MenuItem>
                        ))}
                    </Select>
                </TableCell>
                {headCells.map(headCell => (
                    <TableCell
                        key={headCell.id}
                        onClick={() => createSortHandler(headCell.id)()}
                        className={`${cls.head_table_row_align} ${headCell.disablePadding ? cls.head_table_row_none_padding : cls.head_table_row_padding}`}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                        >
                            {headCell.label}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};

export default EnhancedTableHead;