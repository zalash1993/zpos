import React, { useEffect, useState } from 'react';
import Modal from '../../../../../../components/Modal';
import { Select, MenuItem } from '@material-ui/core';
import { useLocation, useHistory } from 'react-router-dom';
import statusIconGenerator from '../../../../../../helpers/statusIconGenerator';
import { useTranslation } from 'react-i18next';

const ModalFilter = ({ setFilterOpen, filterOpen, setFilterActive, headCells, rows, filterActive, filterRows }) => {
    const { t } = useTranslation();
    const [arrayFilter, setArrayFilter] = useState([]);
    const { search } = useLocation();
    const { push } = useHistory();

    useEffect(() => {
        const newArraySelect = {};
        if (!rows.length) {
            return;
        }
        Object.keys(rows[0]).forEach((key) => {
            if (headCells.find(({ id }) => id === key)) {
                newArraySelect[key] = []
            }
        })
        rows.forEach((el) => {
            Object.keys(el).forEach((key) => {
                if (newArraySelect[key]?.indexOf(el[key]) === -1 && headCells.find(({ id }) => id === key)) {
                    newArraySelect[key].push(el[key])
                }
            })
        });
        setArrayFilter(newArraySelect);
    }, [headCells, rows]);

    useEffect(() => {
        if (search) {
            setFilterActive({ servStatus: search.substr(1) });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const hendlerValueSelect = (headCell) => {
        if (filterActive[headCell.id] === null) {
            return '-'
        }
        if (filterActive[headCell.id]) {
            return filterActive[headCell.id]
        }
        if (arrayFilter[headCell.id]?.length === 1) {
            return arrayFilter[headCell.id][0]
        }
        return 'Все'
    }
    return (
        <Modal open={filterOpen} close={setFilterOpen} titleButtom='сбросить' titleButtom2='применить' buttomClick={() => setFilterActive({})}>
            <h1>Фильтр</h1>
            {headCells.map((headCell, index) => (
                <div key={index} style={{ marginBottom: '20px' }}> {headCell.label}: <Select
                    value={hendlerValueSelect(headCell)}
                    onChange={({ target: { value } }) => {
                        if (search) {
                            push('/equipment');
                        }
                        if (value === 'Все') {
                            const newFilterActive = { ...filterActive }
                            delete newFilterActive[headCell.id]
                            return setFilterActive(newFilterActive)
                        }
                        setFilterActive({ ...filterActive, [headCell.id]: value === "-" ? null : value });
                    }}
                    style={{ width: '100%' }}
                >
                    <MenuItem value={'Все'}>Все</MenuItem>
                    {arrayFilter?.[headCell.id]?.map(el => <MenuItem key={el} name={headCell.id} value={el}>{headCell.icon ? statusIconGenerator(el,null,t) : el}</MenuItem>)}
                </Select>
                </div>
            ))}
            найдено: {filterRows.length} записей
                    </Modal>
    )
}

export default ModalFilter;