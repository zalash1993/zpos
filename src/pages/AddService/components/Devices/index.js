import React, {
    useEffect,
    useState, Fragment
} from 'react';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Paper,
} from '@material-ui/core';
import SortHelper from '../../../../helpers/sortHelper';
import cls from './devices.module.scss';
import EnhancedTableHead from './components/EnhancedTableHead';
import EnhancedTableToolbar from './components/EnhancedTableToolbar';
import hendelClickRow from '../../../../helpers/hendelClickRow';
import HandlerRowDesktopAndMobile from '../../../../components/HandlerRowDesktopAndMobile';
import Loading from '../../../../components/Loading';
import ButtonStyle from '../../../../components/ButtonStyle';
import ModalFilter from './components/ModalFilter';
import EnhancedTablePagination from '../../../../components/EnhancedTablePagination';
import { useHandleRequestSort } from '../../../../helpers/handleRequestSort';
import { useTranslation } from 'react-i18next';

const headCells = [
    { id: 'equTypeName', numeric: false, disablePadding: true, align: 'left', label: 'Тип устройства', bool: false, mobileLine: false },
    { id: 'serialNumber', numeric: true, disablePadding: false, align: 'left', label: `S/N`, bool: false, mobileLine: true },
];

const Devices = ({ rows, prepareAnOrder, resetTable, loading }) => {
    const { t } = useTranslation();
    const [{ order, orderBy }, handleRequestSort] = useHandleRequestSort({
        order: 'asc',
        orderBy: 'equType'
    })
    const [selected, setSelected] = useState([]);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const isSelected = name => selected.indexOf(name) !== -1;

    const emptyRows = 5 - Math.min(5, rows.length - page * 5);

    const [filterOpen, setFilterOpen] = useState(false);
    const [filterActive, setFilterActive] = useState({});

    const handleChangeFilterRows = (array, params) => {
        return array.filter(el => {
            if (Object.keys(params).length) {
                let status = []
                Object.keys(params).forEach(key => {
                    if (el[key] === params[key]) {
                        status.push(true)
                    } else {
                        status.push(false)
                    }
                });
                let finishSearch = false;
                if (status.find(bool => bool === false) === undefined) {
                    finishSearch = true
                }
                return finishSearch;
            }
            return true
        })
    }

    const [newRows,
        setNewRows
    ] = useState(handleChangeFilterRows(rows, filterActive));
    useEffect(() => {
        setNewRows(handleChangeFilterRows(rows, filterActive))
    }, [filterActive, rows]);
    return (<Paper className={cls.equipment_and_services_paper_wrapper}>
        <EnhancedTableToolbar
            filterActive={filterActive}
            setFilterActive={setFilterActive}
            setFilterOpen={() => setFilterOpen(!filterOpen)} numSelected={selected.length} />
        <TableContainer className={cls.equipment_and_services_table_container}>
            <Loading load={loading} />
            <Table
                className={cls.equipment_and_services_table_min}
                aria-labelledby="tableTitle"
                size='medium'
                aria-label="enhanced table"
            >
                <EnhancedTableHead
                    numSelected={selected.length}
                    order={order}
                    orderBy={orderBy}
                    onSelectAllClick={() => {
                        resetTable();
                        hendelClickRow.handleSelectAllRows(selected, newRows, setSelected)
                    }}
                    onRequestSort={handleRequestSort}
                    rowCount={newRows.length}
                    headCells={headCells}
                />
                <ModalFilter
                    setFilterOpen={() => setFilterOpen(false)}
                    filterOpen={filterOpen}
                    setFilterActive={setFilterActive}
                    headCells={headCells}
                    rows={rows}
                    filterActive={filterActive}
                    filterRows={newRows}
                />
                <TableBody>
                    {
                        SortHelper(newRows, order, orderBy)
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((row, index) => {
                                const isItemSelected = isSelected(row);
                                return (
                                    <Fragment key={index}>
                                        <HandlerRowDesktopAndMobile
                                            headCells={headCells}
                                            cursor
                                            handleClick={(event, row) => {
                                                resetTable();
                                                hendelClickRow(event, row, selected, setSelected)
                                            }}
                                            row={row}
                                            checkbox
                                            isItemSelected={isItemSelected}
                                            column
                                            index={index}
                                            minimize
                                        />
                                    </Fragment>
                                );
                            })}
                    {!newRows.length && rows.length ?
                        <TableRow style={{ height: 53 * emptyRows }}>
                            <TableCell colSpan={6}>
                                {t('pages.equipment.noFilter')} <div onClick={() => setFilterActive({})}>{t('pages.equipment.resetFilter')}</div>
                            </TableCell>
                        </TableRow> : null}
                    {emptyRows > 0 && (
                        <TableRow style={{ height: 53 * emptyRows }}>
                            <TableCell colSpan={6} />
                        </TableRow>
                    )}
                </TableBody>
            </Table>
        </TableContainer>
        <EnhancedTablePagination
            count={newRows.length}
            page={page}
            rowsPerPage={rowsPerPage}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}

        />
        {selected.length > 0 && <div className={cls.equipment_and_services_button_block}>
            <ButtonStyle onClick={() => prepareAnOrder(selected)}>{t('pages.equipment.formAnOrder')}</ButtonStyle>
        </div>}
    </Paper>);
}

export default Devices;