import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import services from '../../redux/actions/services';
import cls from './addService.module.scss';
import PrepareAnOrder from '../../components/PrepareAnOrder';
import PrepareOrderPay from '../../components/PrepareOrderPay';
import movementInRef from '../../helpers/movementInRef';
import HelmetComponent from '../../components/HelmetComponent';
import { Select, MenuItem, Card } from '@material-ui/core';
import Devices from './components/Devices';
import { useTranslation } from 'react-i18next';

const AddService = () => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const {
        prepareAnOrder,
        prepareOrderPay,
        isRequested,
        isRequestedAnOrder,
        isRequestedOrderPay,
        serviceList,
        serviceDevices
    } = useSelector((state) => state.services);
    const refPaperAnOrder = useRef();
    const refPaperAnOrderPay = useRef();
    const resetTable = () => {
        dispatch(services.resetOrder());
    };
    const resetBlock = () => {
        dispatch({ type: 'PREPARE_AN_ORDER_REQUEST_SUCCESS', prepareAnOrder: {} })
        dispatch({ type: 'PREPARE_AN_ORDER_PAY_REQUEST_SUCCESS', prepareOrderPay: {} });
    }
    useEffect(() => {
        dispatch(services.serviceList());
        return () => {
            resetTable();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const [selectChange, setSelectChange] = useState(1);
    const [selectOpen, setSelectOpen] = useState(false);
    useEffect(()=>{
        setTimeout(()=>setSelectOpen(true),1000);
    },[])
    return (<div>
        <HelmetComponent titlePage={t('pages.addService.helmet.titlePage')} description={t('pages.addService.helmet.description')} title={t('pages.addService.helmet.title')} />
        <h1>{t('pages.addService.title')}</h1>
        <Card style={{ height: 50, display: 'flex' }}>
            <Select
                open={selectOpen}
                value={selectChange}
                onClose={() => setSelectOpen(false)}
                onOpen={() => setSelectOpen(true)}
                onChange={({ target: { value } }) => {
                    resetTable();
                    setSelectChange(value);
                    if (value !== 1) {
                        dispatch(services.serviceDevices(serviceList.find(({ servType }) => servType === value)));
                    }
                }}
                style={{ width: '100%' }}
            >
                <MenuItem value={1} className={cls.select_item}>
                    {t('pages.addService.title')}
                </MenuItem>
                {serviceList?.length && serviceList?.map(({ servType, servName }, index) => <MenuItem key={index} className={cls.select_item} value={servType}>
                    {servName}
                </MenuItem>)}
            </Select>
        </Card>
        {serviceDevices.length > 0 && <Devices
            resetTable={resetBlock}
            rows={serviceDevices}
            loading={isRequested}
            prepareAnOrder={(params) => dispatch(services.prepareAnOrder(params, null, () => movementInRef(refPaperAnOrder)))}
        />}
        {prepareAnOrder?.terms?.length > 0 && (<div ref={refPaperAnOrder}>
            <h1>{t('general.titleOrder')}</h1>
            <PrepareAnOrder
                loading={isRequestedAnOrder}
                rows={prepareAnOrder.terms}
                prepareAnOrder={(params) => dispatch(services.prepareOrderPay(params, null, () => movementInRef(refPaperAnOrderPay)))}
            />
        </div>)

        }
        {prepareOrderPay?.orderData?.length > 0 && (<div ref={refPaperAnOrderPay}>
            <h1>{t('general.titleOrder2')}</h1>
            <PrepareOrderPay
                loading={isRequestedOrderPay}
                params={prepareOrderPay.orderParams}
                rows={prepareOrderPay.orderData}
                warning={prepareOrderPay.orderWarningDates}
                error={prepareOrderPay.orderErrorDates}
                prepareAnOrder={(params, callback) => dispatch(services.orderPay(params, null, callback))}
                resetOrder={() => dispatch(services.resetOrder())}
                backStep={() => {
                    movementInRef(refPaperAnOrder)
                }}
            />
        </div>)

        }
    </div>)
}

export default AddService;