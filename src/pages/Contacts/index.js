import React from 'react';
import { Container } from '@material-ui/core';
import cls from './contacts.module.scss';
import maps from '../../img/carta.png';
import fone from '../../img/fone.png';
import mts from '../../img/mts.png';
import vel from '../../img/vel.png';
import viber from '../../img/viber.jpg';
import HelmetComponent from '../../components/HelmetComponent';
import { useTranslation } from 'react-i18next';

const Contacts = () => {
    const { t } = useTranslation();
    return (
        <Container component="main" className={cls.container}>
            <HelmetComponent titlePage='Контакты' description='Контакты' title='Контакты' />
            <div className={cls.contacts_home_name}>
                <h1>{t('pages.contacts.title')}</h1>
            </div>
            <div className={cls.contacts_content_wrapper}>
                <div className={cls.contacts_information_block}>
                    <div className={cls.contacts_current_item}>
                        <h5>{t('pages.contacts.LLC')}</h5>
                        <span>{t('pages.contacts.unp')}</span>
                    </div>
                    <div className={cls.contacts_current_item}>
                        <h5>{t('pages.contacts.address')}:</h5>
                        <span> {t('pages.contacts.address1')}</span>
                        <span> {t('pages.contacts.address2')}</span>
                    </div>
                    <div className={cls.contacts_current_item}>
                        <h5> {t('pages.contacts.workingHours')}:</h5>
                        <span>  {t('pages.contacts.salesDepartment')}: {t('general.with')} 9:00 {t('general.before')} 18:00 ({t('general.friday')} {t('general.before')} 16:45)</span>
                        <span> {t('pages.contacts.serviceCenter')}: {t('general.with')} 9:30 {t('general.before')} 17:30 ({t('general.friday')} {t('general.before')} 16:15)</span>
                        <span> {t('pages.contacts.weekend')}: {t('general.saturday')}, {t('general.sunday')}.</span>
                    </div>
                    <div className={cls.contacts_current_item}>
                        <h5>{t('pages.contacts.phoneSales')}:</h5>
                        <span className={cls.contacts_block_call}>  <a href="tel:+375 17 361-61-77"> <img src={fone} alt='домашний телуфон' /> +375 17 361-61-77</a></span>
                        <span className={cls.contacts_block_call}>  <a href="tel:+375 44 585-67-73"> <img src={viber} alt='viber' /> <img src={vel} alt='vel' />+375 44 585-67-73</a></span>
                        <span className={cls.contacts_block_call}>     <a href="tel:+375 29 562-78-62"> <img src={fone} alt='домашний телуфон' /> +375 29 562-78-62</a></span>
                    </div>
                    <div className={cls.contacts_current_item}>
                        <h5>{t('pages.contacts.phoneLine')}:</h5>
                        <span className={cls.contacts_block_call_mini}> <a href="tel:7040"> <img src={vel} alt='vel' /> 7040</a></span>
                        <span className={cls.contacts_block_call_mini}> <a href="tel:7040"> <img src={mts} alt='vel' /> 7040</a></span>
                    </div>
                    <div className={cls.contacts_current_item}>
                        <h5> {t('pages.contacts.email')}:</h5>
                        <span className={cls.contacts_block_call}> <a href="mailto:sales@ecsat-bel.com">sales@ecsat-bel.com</a></span>
                        <span className={cls.contacts_block_call}>  <a href="mailto:service@ecsat-bel.com">service@ecsat-bel.com</a></span>
                    </div>

                </div>
                <div className={cls.contacts_img_block}>
                    <img src={maps} alt='карта' />
                </div>

            </div>
        </Container>
    )
}

export default Contacts;