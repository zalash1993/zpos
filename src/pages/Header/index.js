import React, { useState, useEffect } from 'react'
import { Container } from '@material-ui/core';
import cls from './header.module.scss';
import logo from '../../img/zPOS_logo_RGB_color.png';
import { Link, NavLink, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import authentication from '../../redux/actions/authentication';
import { push as Menu } from 'react-burger-menu';
import swipeHandler from './swipeHandler';
import HamburgerMenu from '../../components/HamburgerMenu';

const Header = () => {
    const { hash } = useLocation();
    useEffect(() => {
        swipeHandler(setButtonBurger)
    }, [])
    const arrayMenu = [{ name: 'Оборудование', path: '/#hardware' },
    { name: 'Программная касса', path: '/#software' },
    { name: 'Сервис', path: '/#service' },
    { name: 'Вопрос/ответ', path: '/#faq' },
    { name: 'Контакты', path: '/#contact' }];
    const [buttonBurger, setButtonBurger] = useState(false);
    const [styleLeft, setStyleLeft] = useState({ left: 0, width: 0,marginLeft:0 });
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(authentication.cardMain());
    }, [dispatch]);

    const handleMouseEnter = (e) => {
        setStyleLeft({ left: e.target.offsetLeft + e.target.offsetWidth/2, width: e.target.offsetWidth,marginLeft: -e.target.offsetWidth/2});
    }

    const menuContent = <>
        {arrayMenu.map((el, index) => (
            <div key={index} className={cls.header_tab} onMouseEnter={handleMouseEnter} onClick={() => {
                setButtonBurger(false);
            }}>
                <NavLink exact className={cls.header_tab_all} activeClassName={hash && el.path.indexOf(hash) + 1 ? cls.header_tab_active : ''} to={el.path}>
                    {el.name}
                </NavLink>
            </div>
        ))}
        <div className={cls.header_tab} onMouseEnter={handleMouseEnter} onClick={() => setButtonBurger(false)}>
            <NavLink to={'/order'} className={cls.header_tab_all_last}>
                Оформить заявку
            </NavLink>
        </div>
    </>
    return (<>
        <header className={cls.header_container}>
            <Container component="main" className={cls.container}>
                <div className={cls.header_logo_container}>
                    <Link to='/' onClick={() => window.scrollTo({ top: 0,behavior: 'smooth' })}>
                        <img src={logo} alt='logo' />
                    </Link>
                </div>
                <div className={cls.header_button_burger}>
                    <div className={cls.menu_wrapper} onClick={() => setButtonBurger(!buttonBurger)}>
                        <HamburgerMenu boolean={buttonBurger} />
                    </div>
                </div>
                <div onMouseLeave={() => setStyleLeft({ ...styleLeft, width: 0, marginLeft:0 })} className={`${cls.header_tabs_container}`}>
                    {menuContent}
                    <span className={cls.header_line} style={styleLeft} />
                </div>
            </Container>
        </header>
        <div className={cls.menu_conatiner_wrapper}>
            <div id="outer-container">
                <Menu outerContainerId={"outer-container"} onStateChange={({ isOpen }) => {
                    setButtonBurger(isOpen);
                }} right isOpen={buttonBurger} customBurgerIcon={false}  >
                    {menuContent}
                </Menu>
            </div>
        </div>
    </>)
}

export default Header;