import React, { useEffect, useState, Fragment } from 'react';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Paper,
} from '@material-ui/core';
import SortHelper from '../../../../helpers/sortHelper';
import cls from './equipmentAndServices.module.scss';
import EnhancedTableHead from './components/EnhancedTableHead';
import EnhancedTableToolbar from './components/EnhancedTableToolbar';
import hendelClickRow from '../../../../helpers/hendelClickRow';
import HandlerRowDesktopAndMobile from '../../../../components/HandlerRowDesktopAndMobile';
import Loading from '../../../../components/Loading';
import ButtonStyle from '../../../../components/ButtonStyle';
import ModalFilter from '../../../../components/ModalFilter';
// import EnhancedTablePagination from '../../../../components/EnhancedTablePagination';
import { useHandleRequestSort } from '../../../../helpers/handleRequestSort';
import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
// import EquipmentSelector from '../../../../redux/Selectors/Equipment';
import services from '../../../../redux/actions/services';
import { useTranslation } from 'react-i18next';
import ItemTableList from './components/ ItemTableList';
import CollapseButton from '../../../../components/CollapseButton';


const EquipmentAndServices = ({ prepareAnOrder, resetTable, loading }) => {
    const { t } = useTranslation();
    const headCells = [
        { id: 'equType', numeric: false, disablePadding: true, align: 'left', label: t('headCells.equType'), bool: false, mobileLine: false },
        { id: 'serialNumber', numeric: true, disablePadding: false, align: 'left', label: t('headCells.serialNumber'), bool: false, mobileLine: true },
    ];
    const rows = useSelector(state => state.services.equipments);
    const { equipmentsLength } = useSelector(state => state.services);
    const history = useHistory();
    const dispatch = useDispatch();
    const [{ order, orderBy }, handleRequestSort] = useHandleRequestSort({
        order: 'asc',
        orderBy: 'equType'
    })
    const [selected, setSelected] = useState([]);
    // const [page, setPage] = useState(0);
    // const [rowsPerPage, setRowsPerPage] = useState(5);
    useEffect(() => {
        dispatch(services.myServices());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    useEffect(() => {
        resetTable()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selected]);
    // const handleChangePage = (event, newPage) => {
    //     setPage(newPage);
    // };

    // const handleChangeRowsPerPage = event => {
    //     setRowsPerPage(parseInt(event.target.value, 10));
    //     setPage(0);
    // };

    const emptyRows = 5 - Math.min(5, rows.length - 0 * 5);

    const [filterOpen, setFilterOpen] = useState(false);
    const [filterActive, setFilterActive] = useState({});

    const handleChangeFilterRows = (array, params) => {
        return array.filter(el => {
            if (Object.keys(params).length) {
                let status = []
                Object.keys(params).forEach(key => {
                    if (el[key] === params[key]) {
                        status.push(true)
                    } else {
                        let statusEl = false;
                        if (key === 'servStatusAll' && Array.isArray(params[key])) {

                            params[key].forEach(parEl => {
                                if (Boolean(el[key].find(elEl => elEl === parEl))) {
                                    statusEl = true;
                                }
                            })
                        }
                        statusEl ? status.push(true) : status.push(false);
                    }
                });
                let finishSearch = false;
                if (status.find(bool => bool === false) === undefined) {
                    finishSearch = true
                }
                return finishSearch;
            }
            return true
        })
    }

    const [newRows, setNewRows] = useState(handleChangeFilterRows(rows, filterActive));
    useEffect(() => {
        setNewRows(handleChangeFilterRows(rows, filterActive))
    }, [filterActive, rows]);
    const [collapse, setCollapse] = useState([]);
    // const [click, setclick] = useState(false);
    return (<>
        <Paper className={cls.equipment_and_services_paper_wrapper}>
            <EnhancedTableToolbar
                filterActive={filterActive}
                setFilterActive={setFilterActive}
                setFilterOpen={() => setFilterOpen(!filterOpen)} numSelected={selected.length} />
            <TableContainer style={{ overflowX: 'inherit' }} className={cls.container_wrapper}>
                <Loading load={loading} />
                <Table
                    className={`${cls.equipment_and_services_table_min} ${cls.equipment_and_services_table_container}`}
                    aria-labelledby="tableTitle"
                    size='medium'
                    aria-label="enhanced table"
                >
                    <EnhancedTableHead
                        numSelected={selected.length}
                        order={order}
                        orderBy={orderBy}
                        onSelectAllClick={() => hendelClickRow.handleSelectAllRowsCollapse(selected, newRows, setSelected)}
                        onRequestSort={handleRequestSort}
                        rowCount={equipmentsLength}
                        headCells={headCells}
                        sort
                        checkbox
                    />
                    <ModalFilter
                        setFilterOpen={() => setFilterOpen(false)}
                        filterOpen={filterOpen}
                        setFilterActive={setFilterActive}
                        headCells={headCells}
                        rows={rows}
                        filterActive={filterActive}
                        filterRows={newRows}
                    />
                    <TableBody>
                        {
                            SortHelper(newRows, order, orderBy)
                                // .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    return (
                                        <Fragment key={index}>
                                            <CollapseButton onClick={() => {
                                                let a = [...collapse]
                                                if (!a.filter(el => el === row.serialNumber).length) {
                                                    a.push(row.serialNumber)
                                                } else {
                                                    a = a.filter(el => el !== row.serialNumber)
                                                }
                                                setCollapse(a)
                                            }} boolean={Boolean(collapse.filter(el => el === row.serialNumber).length)} />
                                            <HandlerRowDesktopAndMobile
                                                headCells={headCells}
                                                handleClick={() => {
                                                    let a = [...collapse]
                                                    if (!a.filter(el => el === row.serialNumber).length) {
                                                        a.push(row.serialNumber)
                                                    } else {
                                                        a = a.filter(el => el !== row.serialNumber)
                                                    }
                                                    setCollapse(a)
                                                }}
                                                row={row}
                                                cursor
                                                checkboxnone
                                                column
                                                index={index}
                                                minimize
                                            />
                                            <ItemTableList
                                                cls={cls}
                                                row={row}
                                                collapse={collapse}
                                                newRows={newRows}
                                                selected={selected}
                                                setSelected={setSelected}
                                            />
                                        </Fragment>
                                    );
                                })}
                        {!newRows.length && rows.length ?
                            <TableRow style={{ height: 53 * 1 }}>
                                <TableCell colSpan={6}>
                                    {t('pages.equipment.noFilter')}
                                    <div className={cls.reset_filter} onClick={() => setFilterActive({})}>{t('pages.equipment.resetFilter')}</div>
                                </TableCell>
                            </TableRow> : null}
                        {emptyRows > 0 && (
                            <TableRow style={{ height: 53 * emptyRows }}>
                                <TableCell colSpan={6} />
                            </TableRow>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            {/* <EnhancedTablePagination
                count={newRows.length}
                page={page}
                rowsPerPage={rowsPerPage}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}

            /> */}
            <div className={cls.equipment_and_services_button_block}>
                {selected.length > 0 ? <ButtonStyle onClick={() => prepareAnOrder(selected)}>{t('pages.equipment.formAnOrder')}</ButtonStyle>
                    : <ButtonStyle onClick={() => history.push('/add-service')}>{t('pages.equipment.addService')}</ButtonStyle>}
            </div>
        </Paper>
        {/* {!selected.length && <div style={{ marginBottom: 16, textAlign: 'end' }} >
            <ButtonStyle onClick={() => history.push('/add-service')}>{t('pages.equipment.addService')}</ButtonStyle>
        </div>} */}
    </>);
}

export default EquipmentAndServices;