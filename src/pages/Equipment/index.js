import React, { useEffect, useRef } from 'react';
import EquipmentAndServices from './component/EquipmentAndServices/EquipmentAndServices';
import { useDispatch, useSelector } from 'react-redux';
import services from '../../redux/actions/services';
import cls from './equipment.module.scss';
import PrepareAnOrder from '../../components/PrepareAnOrder';
import PrepareOrderPay from '../../components/PrepareOrderPay';
import movementInRef from '../../helpers/movementInRef';
// import EquipmentSelector from '../../redux/Selectors/Equipment';
import HelmetComponent from '../../components/HelmetComponent';
import { useTranslation } from 'react-i18next';

const Equipment = () => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const { prepareAnOrder, prepareOrderPay, isRequested, isRequestedAnOrder, isRequestedOrderPay } = useSelector((state) => state.services);
 
    const refPaperAnOrder = useRef();
    const refPaperAnOrderPay = useRef();
    const resetTable = () => {
        dispatch(services.resetOrder());
    };
    useEffect(() => {
        return () => {
            resetTable();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (<div className={cls.equipment_container_wrapper}>
        <HelmetComponent titlePage={t('pages.equipment.helmet.titlePage')} description={t('pages.equipment.helmet.description')} title={t('pages.equipment.helmet.title')} />
        <EquipmentAndServices
            resetTable={resetTable}
            loading={isRequested}
            prepareAnOrder={(params) => dispatch(services.prepareAnOrder(params, null, () => movementInRef(refPaperAnOrder)))}
        />
        {prepareAnOrder?.terms?.length > 0 && (<div ref={refPaperAnOrder}>
            <h1>{t('general.titleOrder')}</h1>
            <PrepareAnOrder
                loading={isRequestedAnOrder}
                rows={prepareAnOrder.terms}
                prepareAnOrder={(params) => dispatch(services.prepareOrderPay(params, null, () => movementInRef(refPaperAnOrderPay)))}
            />
        </div>)

        }
        {prepareOrderPay?.orderData?.length > 0 && (<div ref={refPaperAnOrderPay}>
            <h1>{t('general.titleOrder2')}</h1>
            <PrepareOrderPay
                loading={isRequestedOrderPay}
                params={prepareOrderPay.orderParams}
                rows={prepareOrderPay.orderData}
                warning={prepareOrderPay.orderWarningDates}
                error={prepareOrderPay.orderErrorDates}
                prepareAnOrder={(params, callback) => dispatch(services.orderPay(params, null, callback))}
                resetOrder={() => dispatch(services.resetOrder())}
                backStep={() => {
                    movementInRef(refPaperAnOrder)
                }}
            />
        </div>)

        }
    </div>)
}

export default Equipment;