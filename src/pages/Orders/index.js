import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import cls from './orders.module.scss';
import AllOrders from './components/AllOrders';
import PrepareAnOrder from '../../components/PrepareAnOrder';
import PrepareOrderPay from '../../components/PrepareOrderPay';
import orders from '../../redux/actions/orders';
import services from '../../redux/actions/services';
import movementInRef from '../../helpers/movementInRef';
import HelmetComponent from '../../components/HelmetComponent';
import { useTranslation } from 'react-i18next';

const Orders = () => {
    const { t } = useTranslation();
    const { prepareAnOrder, prepareOrderPay, isRequestedAnOrder, isRequestedOrderPay } = useSelector((state) => state.services);
    const refPaperAnOrder = useRef();
    const refPaperAnOrderPay = useRef();
    const dispatch = useDispatch();
    const resetTable = () => {
        dispatch(services.resetOrder());
    };
    useEffect(() => {
        dispatch(orders.myOrders());
        return () => {
            resetTable()
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dispatch]);
    const arrayAllOrders = useSelector(state => state.orders.orders);

    return (<div className={cls.orders_container_wrapper}>
        <HelmetComponent titlePage='Мои заказы' description='Мои заказы' title='Мои заказы' />
        <AllOrders
            resetTable={resetTable}
            rows={arrayAllOrders}
            prepareAnOrder={(orderId) => dispatch(services.prepareAnOrder(null, orderId, () => movementInRef(refPaperAnOrder)))} />
        {prepareAnOrder?.terms?.length > 0 && (<div ref={refPaperAnOrder}>
            <h1>{t('general.titleOrder')}</h1>
            <PrepareAnOrder
                rows={prepareAnOrder.terms}
                // date={prepareAnOrder.orderParams.endDate}
                loading={isRequestedAnOrder}
                flagAuto={prepareAnOrder.orderParams.flagAuto}
                prepareAnOrder={(params) => dispatch(services.prepareOrderPay(params, prepareAnOrder.orderParams.orderId, () => movementInRef(refPaperAnOrderPay)))} />
        </div>)}
        {prepareOrderPay?.orderData?.length > 0 && (<div ref={refPaperAnOrderPay}>
            <h1>{t('general.titleOrder2')}</h1>
            <PrepareOrderPay
                loading={isRequestedOrderPay}
                params={prepareOrderPay.orderParams}
                rows={prepareOrderPay.orderData}
                warning={prepareOrderPay.orderWarningDates}
                error={prepareOrderPay.orderErrorDates}
                prepareAnOrder={(params, callback) => dispatch(services.orderPay(params, prepareAnOrder.orderParams.orderId, callback))}
                resetOrder={() => dispatch(services.resetOrder())}
                backStep={() => movementInRef(refPaperAnOrder)}
            />
        </div>)

        }
    </div>)
}

export default Orders;