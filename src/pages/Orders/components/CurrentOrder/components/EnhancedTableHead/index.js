import React from 'react';
import {
    TableCell,
    TableHead,
    TableRow,
} from '@material-ui/core';
import cls from './style.module.scss';

const EnhancedTableHead = ({headCells}) => {
    return (
        <TableHead className={cls.head_visable}>
            <TableRow>
                {headCells.map(headCell => (
                    <TableCell
                        key={headCell.id}
                        align='center'
                    >
                        {headCell.label}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}


export default EnhancedTableHead;