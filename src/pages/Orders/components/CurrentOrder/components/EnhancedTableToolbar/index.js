import React from 'react';
import {
    Toolbar,
    Typography,
} from '@material-ui/core';
import cls from './style.module.scss';
import { useTranslation } from 'react-i18next';


const EnhancedTableToolbar = ({ orderNum }) => {
    const { t } = useTranslation();
    return (
        <Toolbar>
            <Typography className={cls.prepare_order_title} variant="h6" id="tableTitle">
                <div>{`${t('pages.orders.currentTitle')}${orderNum}`}</div>
            </Typography>
        </Toolbar>
    );
};


export default EnhancedTableToolbar;