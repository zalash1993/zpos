import React, { useState, useEffect, Fragment } from 'react';
import {
    Table,
    TableBody,
    TableContainer,
    Paper,
    Switch
} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import orders from '../../../../redux/actions/orders';
import services from '../../../../redux/actions/services';
import cls from './currentOrder.module.scss';
import EnhancedTableToolbar from './components/EnhancedTableToolbar';
import EnhancedTableHead from './components/EnhancedTableHead';
import HandlerRowDesktopAndMobile from '../../../../components/HandlerRowDesktopAndMobile';
import ButtonStyle from '../../../../components/ButtonStyle';
import { useTranslation } from 'react-i18next';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';


const CurrentOrder = ({ row, orderNum, flagAuto, prepareAnOrder, checkBoxStatus }) => {
    const { t } = useTranslation();
    const headCells = [
        { id: 'equType', numeric: false, disablePadding: true, align: 'left', label: t('headCells.equType'), bool: false, mobileLine: false },
        { id: 'serialNumber', numeric: true, disablePadding: false, align: 'left', label: t('headCells.serialNumber'), bool: false, mobileLine: true },
        { id: 'servName', numeric: true, disablePadding: false, align: 'left', label: t('headCells.servName'), bool: false, mobileLine: false },
        { id: 'servPriceMM', numeric: true, disablePadding: false, align: 'right', label: t('headCells.price_perMM'), bool: false, mobileLine: true, prefix: t('rub') },
        { id: 'servPrice', numeric: true, disablePadding: false, align: 'right', label: t('headCells.servPrice'), bool: false, mobileLine: true, prefix: t('rub') },
    ];
    const [checked, setChecked] = useState(Boolean(flagAuto));
    const dispatch = useDispatch();
    const rows = useSelector(state => state.orders.order);
    const { orderUpdate } = useSelector(state => state.orders);
    useEffect(() => {
        setChecked(Boolean(flagAuto));
    }, [flagAuto]);
    useEffect(() => () => {
        dispatch({
            type: 'ORDER_UPDATE_REQUEST_SUCCESS', orderUpdate: {
                error: 0
            }
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    useEffect(() => {
        dispatch(orders.currentMyOrder(row));
    }, [dispatch, row]);
    const HeandlerClick = () => {
        if (Boolean(flagAuto) !== checked) {
            const params = {
                orderParams: {
                    flagAuto: checked ? 1 : 0,
                }
            }
            dispatch(services.currentOrderUpdate(params, row, orderNum, () => setChecked(Boolean(flagAuto))));
        } else {
            prepareAnOrder();
        }
    }
    return (
        <div className={cls.prepare_order_container}>
            <Paper className={cls.prepare_order_paper}>
                <EnhancedTableToolbar orderNum={orderNum} />
                <TableContainer style={{ overflowX: "hidden" }}>
                    <Table
                        className={cls.prepare_order_table}
                        aria-labelledby="tableTitle"
                        size='medium'
                        aria-label="enhanced table"
                    >
                        <EnhancedTableHead headCells={headCells} />
                        <TableBody>
                            {rows.map((row, index) => {
                                return (
                                    <Fragment key={index}>
                                        <HandlerRowDesktopAndMobile
                                            headCells={headCells}
                                            row={row}
                                            column
                                            index={index}
                                            minimize
                                        />
                                    </Fragment>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <div className={cls.hint_and_switch_wrapper}>
                    <div>
                        <Switch
                            checked={checked}
                            disabled={!checkBoxStatus}
                            onChange={() => {
                                setChecked(!checked);
                            }}
                            className={cls.switch_style_active}
                        />
                        {t('general.autoRepeat')}
                    </div>
                </div>
                <div className={cls.button_wrapper}>
                    <ButtonStyle onClick={HeandlerClick}> {Boolean(flagAuto) !== checked ? t('general.save') : t('general.repeat')}</ButtonStyle>
                </div>
                {Boolean(orderUpdate.error) && orderUpdate?.orderErrorDates?.map((el, index) => (
                    <div key={index} className={cls.prepare_order_pay_error_message}>
                        <HighlightOffIcon /> {t('general.crossesWithOrder')}{el.orderNum} {t('general.from')} {el.servDateStart} {t('general.by2')} {el.servDateStop} {Boolean(el.flagAuto) ? t('general.repeatAttribute') : ''}
                    </div>))}
            </Paper>
        </div>
    );
}
export default CurrentOrder;