import React, { Fragment, useEffect, useRef, useState } from 'react';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Paper,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
} from '@material-ui/core';
import SortHelper from '../../../../helpers/sortHelper';
import cls from './orders.module.scss';
import CurrentOrder from '../CurrentOrder';
import checkToggleStatus from '../../../../helpers/checkToggleStatus';
import HandlerRowDesktopAndMobile from '../../../../components/HandlerRowDesktopAndMobile';
import movementInRef from '../../../../helpers/movementInRef';
import EnhancedTableToolbar from '../../../../components/EnhancedTableToolbar';
import EnhancedTableHead from '../../../../components/EnhancedTableHead';
import ModalFilter from '../../../../components/ModalFilter';
import EnhancedTablePagination from '../../../../components/EnhancedTablePagination';
import { useHandleRequestSort } from '../../../../helpers/handleRequestSort';
import { useTranslation } from 'react-i18next';
import ButtonStyle from '../../../../components/ButtonStyle';
import { useHistory } from 'react-router-dom';
import CollapseButton from '../../../../components/CollapseButton';




const AllOrders = ({ rows, prepareAnOrder, resetTable }) => {
    const { t } = useTranslation();
    const history = useHistory();
    const headCells = [
        { id: 'orderStatus', numeric: true, disablePadding: false, align: "left", label: t('headCells.servStatus'), bool: false, hint: false, doubleIcon: true },
        { id: 'orderNum', numeric: false, disablePadding: true, align: "right", label: t('headCells.orderNo'), bool: false, hint: false },
        { id: 'orderDateStart', numeric: true, disablePadding: false, align: "right", label: t('general.with'), bool: false, hint: false },
        { id: 'orderDateStop', numeric: true, disablePadding: false, align: "right", label: t('general.by2'), bool: false, hint: false },
        { id: 'flagAuto', numeric: false, disablePadding: true, align: "left", label: t('general.autoRepeat'), bool: true, hint: false },
        { id: 'cnt', numeric: true, disablePadding: false, align: "right", label: t('headCells.numberSubscriptions'), bool: false, hint: false },
        { id: 'orderPrice', numeric: true, disablePadding: false, align: "right", label: t('headCells.cost'), bool: false, prefix: t('rub') },
    ];
    const [{ order, orderBy }, handleRequestSort] = useHandleRequestSort({
        order: 'desc',
        orderBy: 'orderNum'
    });
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const emptyRows = 5 - Math.min(5, rows.length - page * 5);

    const [expanded, setExpanded] = useState(false);
    const handleChange = panel => {
        setExpanded(expanded !== panel ? panel : false);
    };
    useEffect(() => {
        resetTable();
        if (expanded) movementInRef(currentOrderRef);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [expanded]);
    const currentOrderRef = useRef();


    const [filterOpen, setFilterOpen] = useState(false);
    const [filterActive, setFilterActive] = useState({});

    const handleChangeFilterRows = (array, params) => {
        if(params.orderNum){
            params.orderNum = Number(params.orderNum);
        }
        if(params.flagAuto){
            params.flagAuto = Number(params.flagAuto);
        }
        return array.filter(el => {
            if (Object.keys(params).length) {
                let status = []
                Object.keys(params).forEach(key => {
                    if (el[key] === params[key]) {
                        status.push(true)
                    } else {
                        status.push(false)
                    }
                });
                let finishSearch = false;
                if (status.find(bool => bool === false) === undefined) {
                    finishSearch = true
                }
                return finishSearch;
            }
            return true
        })
    }

    return (
        <>
            <Paper className={cls.orders_expansion_paper_wrapper}>
                <EnhancedTableToolbar setFilterActive={setFilterActive} filterActive={filterActive} setFilterOpen={() => setFilterOpen(!filterOpen)} />
                <TableContainer style={{ overflowX: 'visible' }}>
                    <Table
                        className={cls.orders_expansion_table_min}
                    >
                        <EnhancedTableHead
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={handleRequestSort}
                            headCells={headCells}
                        />
                        <ModalFilter
                            setFilterOpen={() => setFilterOpen(false)}
                            filterOpen={filterOpen}
                            setFilterActive={setFilterActive}
                            headCells={headCells}
                            rows={rows}
                            filterActive={filterActive}
                            filterRows={handleChangeFilterRows(rows, filterActive)}
                        />
                        <TableBody>
                            {
                                SortHelper(handleChangeFilterRows(rows, filterActive), order, orderBy)
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((row, index) => {
                                        return (<Fragment key={index}>
                                            <CollapseButton onClick={() =>handleChange(row.orderNum) } boolean={expanded !== row.orderNum} />
                                            <HandlerRowDesktopAndMobile
                                                headCells={headCells}
                                                handleClick={(event, row) => handleChange(row.orderNum)}
                                                row={row}
                                                index={index}
                                                cursor
                                            />
                                            <TableRow>
                                                <TableCell colSpan={7} padding="none">
                                                    <ExpansionPanel expanded={expanded === row.orderNum}>
                                                        <ExpansionPanelSummary
                                                            className={cls.orders_expansion_panel}
                                                        />
                                                        <ExpansionPanelDetails>
                                                            {expanded === row.orderNum &&
                                                                <div ref={currentOrderRef} style={{ width: '100%' }}>
                                                                    <CurrentOrder
                                                                        prepareAnOrder={() => prepareAnOrder(row.orderId)}
                                                                        orderNum={row.orderNum}
                                                                        row={row.orderId}
                                                                        flagAuto={row.flagAuto}
                                                                        checkBoxStatus={checkToggleStatus(row.orderStatus)}
                                                                    /></div>}
                                                        </ExpansionPanelDetails>
                                                    </ExpansionPanel>
                                                </TableCell>
                                            </TableRow>
                                        </Fragment>
                                        );
                                    })}
                            {!handleChangeFilterRows(rows, filterActive).length && rows.length ?
                                <TableRow style={{ height: 53 * emptyRows }}>
                                    <TableCell colSpan={7}>
                                        {t('pages.equipment.noFilter')}
                                        <div className={cls.reset_filter} onClick={() => setFilterActive({})}>{t('pages.equipment.resetFilter')}</div>
                                    </TableCell>
                                </TableRow> : null}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: 53 * emptyRows }}>
                                    <TableCell colSpan={7} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <EnhancedTablePagination
                    count={handleChangeFilterRows(rows, filterActive).length}
                    page={page}
                    rowsPerPage={rowsPerPage}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}

                />
            </Paper>
            <div style={{ marginBottom: 16, textAlign: 'end' }} >
                <ButtonStyle onClick={() => history.push('/equipment')}>{t('pages.equipment.equipment')}</ButtonStyle>
            </div>
        </>
    );
};

export default AllOrders;