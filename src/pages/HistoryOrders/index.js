import React, { useEffect, useState, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import historyActions from '../../redux/actions/historyActions';
import { TableContainer, Paper, Table, TableBody, TableCell, TableRow } from '@material-ui/core';
import cls from './historyOrders.module.scss';
import SortHelper from '../../helpers/sortHelper';
import HandlerRowDesktopAndMobile from '../../components/HandlerRowDesktopAndMobile';
import EnhancedTableHead from './components/EnhancedTableHead';
import Loading from '../../components/Loading';
import EnhancedTableToolbar from './components/EnhancedTableToolbar';
import HelmetComponent from '../../components/HelmetComponent';
import EnhancedTablePagination from '../../components/EnhancedTablePagination';
import { useHandleRequestSort } from '../../helpers/handleRequestSort';
import { useTranslation } from 'react-i18next';


const HistoryOrders = () => {
    const { t } = useTranslation();
    const headCells = [
        { id: 'operDt', numeric: true, disablePadding: false, align: 'center', label: t('headCells.dateOfOperation'), bool: false, mobileLine: true, iconImg: true, date: true },
        { id: 'orderNum', numeric: true, disablePadding: false, align: 'left', label: t('headCells.typeOfTransaction'), bool: false, mobileLine: false, orderNum: true, link: true },
        { id: 'operAmount', numeric: false, disablePadding: true, align: 'right', label: t('headCells.amount'), bool: false, mobileLine: true, prefix: t('rub') },
    ];
    const dispatch = useDispatch();
    const {
        isRequested,
        myHistory
    } = useSelector(state => state.historyReducer);
    useEffect(() => {
        dispatch(historyActions.historyOperation());
    }, [dispatch]);
    const [{ order, orderBy }, handleRequestSort] = useHandleRequestSort({
        order: 'desc',
        orderBy: 'operDt'
    })
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [rows, setRows] = useState(myHistory);
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };
    return (<Paper className={cls.history_orders_paper_wrapper}>
        <HelmetComponent titlePage={t('pages.history.helmet.titlePage')} description={t('pages.history.helmet.description')} title={t('pages.history.helmet.title')} />
        <EnhancedTableToolbar rows={myHistory} setRows={setRows} />
        <TableContainer className={cls.history_table_container}>
            <Loading load={isRequested} />
            <Table>
                <EnhancedTableHead
                    order={order}
                    orderBy={orderBy}
                    onRequestSort={handleRequestSort}
                    headCells={headCells}
                />
                <TableBody>
                    {
                        SortHelper(rows, order, orderBy)
                            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((row, index) => {
                                return (
                                    <Fragment key={index}>
                                        <HandlerRowDesktopAndMobile
                                            headCells={headCells}
                                            row={row}
                                            column
                                            index={index}
                                            minimize
                                        />
                                    </Fragment>
                                );
                            })}
                    {!rows.length ?
                        <TableRow style={{ height: 53}}>
                            <TableCell colSpan={6}>
                                {t('pages.equipment.noFilter')}
                            </TableCell>
                        </TableRow> : null}
                </TableBody>
            </Table>
        </TableContainer>
        <EnhancedTablePagination
            count={rows.length}
            page={page}
            rowsPerPage={rowsPerPage}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}

        />
    </Paper>)
}

export default HistoryOrders;