import React, { useState, useEffect } from 'react';
import {
    Toolbar,
    Typography,
} from '@material-ui/core';
import cls from './style.module.scss';
import DataPickerComponent from '../../../../components/DataPickerComponent';
import { useTranslation } from 'react-i18next';


const EnhancedTableToolbar = ({ rows, setRows }) => {
    const { t } = useTranslation();
    const [dateStart, setDateStart] = useState(new Date(new Date().setMonth(new Date().getMonth() - 3)));
    const [dateFinish, setDateFinish] = useState(new Date());
    useEffect(() => {
        const newRows = rows.filter(({ operDt }) => {
            const currentDate = new Date(operDt.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1'));
            if (currentDate.getMonth() >= dateStart.getMonth() && currentDate.getMonth() <= dateFinish.getMonth()) {
                return true
            }
            return false
        });
        setRows([...newRows]);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dateFinish, dateStart, rows]);
    return (
        <Toolbar>
            <Typography className={cls.prepare_order_title} variant="h6" id="tableTitle">
                <span className={cls.title}>{t('pages.history.title')}</span>
                <div className={cls.date_block_wrapper}>
                    {t('general.with')} <DataPickerComponent maxDate={dateFinish} month showMonthYearPicker minDate selected={dateStart} onChange={setDateStart} />
                    {t('general.by2')} <DataPickerComponent month showMonthYearPicker maxDate={new Date()} minDate={dateStart} selected={dateFinish} onChange={setDateFinish} />
                </div>
            </Typography>
        </Toolbar>
    );
};


export default EnhancedTableToolbar;