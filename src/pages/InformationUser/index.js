import React, { useEffect, useRef } from 'react';
import cls from './information.module.scss';
import HelmetComponent from '../../components/HelmetComponent';
import { useTranslation } from 'react-i18next';
import MainPart from './component/MainPart';
import Hardware from './component/Hardware';
import Software from './component/Software';
import Contacts from './component/Contacts';
import Faq from './component/Faq';
import Service from './component/Service';
import { useLocation } from 'react-router-dom';
import movementInRef from '../../helpers/movementInRef';

const InformationUser = () => {
    const { t } = useTranslation();
    const { hash } = useLocation();
    const hardware = useRef();
    const software = useRef();
    const service = useRef();
    const faq = useRef();
    const contact = useRef();
    const refArr = { hardware, software, service, faq, contact };
    useEffect(() => {
        const ref = refArr[hash.slice(1)];
        if (ref) movementInRef(ref);
    }, [hash, refArr]);
    return (
        <main className={cls.information_container}>
            <HelmetComponent titlePage={t('pages.home.helmet.titlePage')} description={t('pages.home.helmet.description')} title={t('pages.home.helmet.title')} />
            <MainPart />
            <div ref={hardware}>
                <Hardware />
            </div>
            <div ref={software}>
                <Software />
            </div>
            <div ref={service}>
                <Service />
            </div>
            <div ref={faq}>
                <Faq />
            </div>
            <div ref={contact}>
                <Contacts />
            </div>
        </main>)
};

export default InformationUser;