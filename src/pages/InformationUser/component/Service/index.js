import React from 'react';
import cls from './service.module.scss';
import { Container } from '@material-ui/core';
import ButtonStyle from '../../../../components/ButtonStyle';
import block1 from '../../../../img/img_service/block1.png';
import block2 from '../../../../img/img_service/block2.png';
import block3 from '../../../../img/img_service/block3.png';
import block4 from '../../../../img/img_service/block4.png';
import block5 from '../../../../img/img_service/block5.png';
// import block6 from '../../../../img/img_service/block6.png';
// import block7 from '../../../../img/img_service/block7.png';
import mts from '../../../../img/mts.png';
import vel from '../../../../img/vel.png';

const Service = () => {
    return (
        <div className={cls.service_container}>
            <Container style={{ position: 'relative' }} component="main" maxWidth="lg">
                <div className={cls.service_block_card}>
                    <h1><span>Сервис</span></h1>
                    <div className={cls.service_top_block_wrapper}>
                        <div className={cls.service_top_block_li_wrapper}>
                            <h4>Сервисный центр предлагает полный спектр услуг по обслуживанию zPOS.Терминала:</h4>
                            <ul className={cls.service_top_block_li}>
                                <li> ввод в эксплуатацию и обучение (заказывается при заключении договора купли-продажи или включены в абонентский договор сервисного обслуживания);</li>
                                <li>гарантийный ремонт;</li>
                                <li>послегарантийный, а также ремонт в негарантийных случаях на основе отдельного договора;</li>
                                <li>комплексное обслуживание клиентов на основании <strong>абонентского сервисного договора.</strong></li>
                            </ul>
                        </div>
                        <div className={cls.service_bottom_block_wrapper}>
                            <div className={cls.service_block_number}>
                                <spa>Единая платная телефонная линия технической поддержки:</spa>
                                <span className={cls.contacts_block_call_mini}> <a href="tel:7040"> <img src={vel} alt='vel' /> 7040</a></span>
                                <span className={cls.contacts_block_call_mini}> <a href="tel:7040"> <img src={mts} alt='vel' /> 7040</a></span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <h3>
                            Заключив Абонентский сервисный договор на комплексное обслуживание вы получите:
                        </h3>
                        <div className={cls.service_block_img_wrapper}>
                            <div className={cls.service_block_img}>
                                <div className={cls.service_img}>
                                    <span className={cls.right}>
                                        Подменное оборудование на время ремонта
                                    </span>
                                    <img src={block1} alt='1' />
                                </div>
                                <div className={cls.service_img}>
                                    <span className={cls.right}>
                                        Ремонт в негарантийных случаях*
                                    </span>
                                    <img src={block2} alt='1' />
                                </div>
                            </div>
                            <div className={cls.service_block_img_middle}>
                                <img src={block5} alt='1' />
                            </div>
                            <div className={cls.service_block_img}>
                                <div className={cls.service_img}>
                                    <img src={block3} alt='1' />
                                    <span className={cls.left}>
                                        Ввод в эксплуатацию и обучение, технические консультации
                                    </span>
                                </div>
                                <div className={cls.service_img}>
                                    <img src={block4} alt='1' />
                                    <span className={cls.left}>
                                        Доставку и возврат оборудования
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h5>Стоимость услуги - <strong>18 рублей в месяц.</strong> Договор заключается на срок от 1 года.</h5>
                            <div className={cls.service_block_button}><ButtonStyle>Заключить договор</ButtonStyle></div>
                            <div className={cls.service_block_order_text}>* - оплата запчастей не входит в стоимость услуги</div>
                        </div>
                    </div>
                </div>
            </Container>
        </div>)
};

export default Service;