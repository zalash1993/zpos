/* eslint-disable no-octal-escape */
import React, { useState } from 'react';
import cls from './faq.module.scss';
import { Container, Collapse } from '@material-ui/core';
import block1 from '../../../../img/img_faq/block1.png';
import CollapseButton from '../../../../components/CollapseButton';
import { Link } from 'react-router-dom';
import block2 from '../../../../img/img_faq/block2.png';

const Faq = () => {
    const FaqArray = [
        {
            title: 'Что такое ZPOS?',
            description: (<>
                <span className={cls.faq_text_block}>
                    <strong>zPOS</strong> – семейство кассовых решений (оборудование и программное обеспечение), реализующих функции программной кассы (работа без СКНО) согласно <a href="http://www.nalog.gov.by/ru/programmed-cash-desk/" target="_blank" rel="noopener noreferrer">требованиям законодательства Республики Беларусь</a>.
                </span>
                <span className={cls.faq_text_block}>
                    <strong>zPOS.Терминалы</strong> – устройства нового поколения, которые совмещают в себе функции банковского терминала (бесконтактная оплата, магнитная полоса, чипы), кассы и сканера штрих-кодов. Благодаря компактным размерам, быстрой скорости обмена информацией и длительной работе без подзарядки, устройства незаменимы для небольших магазинов, служб доставки и другого вида деятельности.


                </span>
                <span className={cls.faq_text_block}>
                    <strong>zPOS.Касса</strong> – специализированное программное обеспечение на базе Android, устанавливаемое на zPOS.Терминал, и разработанное для совершения кассовых операций (наличный и безналичный расчет), учета товаров и иных функций.
                </span>
            </>)
        }, {
            title: 'ЧТО НЕОБХОДИМО ДЛЯ НАЧАЛА РАБОТЫ?',
            description: (<>
                <span className={cls.faq_text_block}>
                    Для начала работы с zPOS в Республике Беларусь Вам необходимо:
                </span>
                <ol>
                    <li><Link to="/order">Оформить заявку</Link> для заключения договора на покупку zPOS.Терминала и абонентского договора на использование zPOS.Касса.</li>
                    <li>Заключить договор с банком на оказание услуги эквайринга для возможности приема платежей с использованием банковских карточек;</li>
                    <li>Заключить договор с оператором программной кассовой системы на информационное обслуживание и СКО (средство контроля оператора); </li>
                    <li>Заключить договор с РУП "Информационно-издательский центр по налогам и сборам" на информационное обслуживание; </li>
                    <li>Для обмена данными с банком и оператором программной кассовой системы обеспечить доступ zPOS.Терминалу в сеть Интернет. В зависимости от модели zPOS.Терминалы поддерживают 3G\4G, WIFI, Ethernet.</li>
                </ol>
            </>)
        }, {
            title: 'КАКИЕ ВОЗМОЖНЫ СПОСОБЫ ОПЛАТЫ?',
            description: (<>
                <span className={cls.faq_text_block}>
                    Наши банковские реквизиты для оплаты, включая номер Вашего Лицевого счета, будут направлены Вам на email после оформления Вами заявки на оборудование/услуги.
                </span>
                <span className={cls.faq_text_block}>
                    Оплата за zPOS.Терминал, дополнительное оборудование и услуги производится на наш расчетный счет следующими способами:
                </span>

                <ul>
                    <li>платежным поручением в банке или в системе дистанционного банковского обслуживания (Клиент-банк, интернет-, мобильный банкинг и проч.);</li>
                    <li>наличными через кассу банка.</li>
                </ul>
                <span className={cls.faq_text_block}>
                    <strong>Оплата за услуги доступа к ПО zPOS.Касса производится на наш расчетный счет с обязательным указанием Вашего номера Лицевого счета следующими способами:</strong>
                </span>
                <ul>
                    <li>платежным поручением в банке или в системе дистанционного банковского обслуживания;</li>
                    <li>наличными через кассу банка;</li>
                    <li>через АИС ”Расчет“ (ЕРИП) в банках и отделениях РУП «Белпочта», в системе дистанционного банковского обслуживания, с использованием наличных, электронных денег (WebMoney, belqi) и банковских карточек. Оплата карточками иностранных банков в системе «Расчет» доступна на платежном ресурсе e-pay.by.</li>
                </ul>
                <div className={cls.img_block}>
                    <img src={block2} alt='1' />
                </div>
                <span className={cls.faq_text_block}>
                    <strong>Для оплаты за услуги доступа к ПО zPOS.Касса через АИС ”Расчет“ (ЕРИП) необходимо:</strong>
                </span>
                <ol>
                    <li>Выбрать раздел «IT услуги» → «Эксат-Лаб» → «Доступ к ПО | Код услуги 4713941» или отсканировать QR-код;</li>
                    <li>Ввести номер лицевого счета;</li>
                    <li>Ввести сумму платежа (если не указана); </li>
                    <li>Проверить корректность информации (будет отображена на экране после ввода № лицевого счета);</li>
                    <li>Подтвердить совершение платежа.</li>
                </ol>
                <span className={cls.faq_text_block}>
                    <strong>Обращаем Ваше внимание, что задержка в поступлении денежных средств на счет может составлять до 5 банковских дней в зависимости от способа оплаты!</strong>
                </span>
            </>)
        }, {
            title: 'Возможна ли доставка?',
            description: (<>
                <span className={cls.faq_text_block}>
                    <strong>Стоимость доставки (бел.руб с НДС):</strong>
                </span>
                <ul>
                    <li>по г.Минск - 7.20;</li>
                    <li>по РБ - 21.60.</li>
                </ul>
                <span className={cls.faq_text_block}>
                    <strong>Срок и адрес доставки:</strong>
                </span>

                <span className={cls.faq_text_block}>
                    в течение 10 рабочих дней после получения предоплаты в заранее оговоренные с Вами дату и время (только во второй половине рабочего дня) по заранее согласованному с Вами адресу, указанному в счет-договоре;
                </span>
                <span className={`${cls.faq_text_block} ${cls.faq_text_block_red}`}>
                    Просим Вас находиться на месте в согласованные дату и время!
                </span>
                <span className={cls.faq_text_block}>
                    <strong>Действия при получение товара:</strong>
                </span>
                <ol>
                    <li>Подписать 2 экземпляра счет-договора и 2 экземпляра ТН/ТТН, вторые экземпляры необходимо вернуть курьеру. (В обязанности курьеров не входит консультация и установка оборудования)</li>
                    <li>Претензии к внешнему виду и комплектации товара после подписания ТН/ТТН и отъезда курьера не принимаются;</li>
                    <li>Настоятельно рекомендуем проверить наименование, серийный номер и комплектацию товара до подписания ТН/ТТН!</li>
                </ol>
            </>)
        }, {
            title: 'Какую модель zPOS.Терминала выбрать?',
            description: (<>
                <span className={cls.faq_text_block}>
                    Основное отличие моделей zPOS.Терминалов друг от друга заключается в их размере, автономности (наличии встроенного аккумулятора), наличии считывателей банковских карт.
                </span>

                <ul>
                    <li style={{ marginBottom: 20 }}>
                        <span className={cls.faq_text_block}>
                            <strong>Мобильные</strong> кассы предназначены в первую очередь для мобильной торговли, служб доставки. Кроме своих компактных размеров, могут иметь встроенные средства для чтения банковских карточек всех типов – магнитных, чиповых, бесконтактных (уточняйте в разделе описания конкретной кассы).
                </span></li>
                    <li><span className={cls.faq_text_block}>
                        <strong>Стационарные</strong> кассы. Могут взаимодействовать в едином цикле с <a href="https://ecsat-pos.by/detailterminal_mpos" target="_blank" rel="noopener noreferrer">mPOS-терминалом</a> для приема платежей по банковским картам.
                </span></li>
                </ul>
                <span className={cls.faq_text_block}>
                    <strong>Сфера применения программных касс:</strong> сфера торговли, общественного питания (не обеспечивает выдачу счета и может использоваться в объектах общественного питания без обслуживания потребителей официантами (барменами) за столиками), услуг (кроме автомобильных перевозок пассажиров, автозаправочных станций), за исключением случаев, когда в этих сферах в соответствии с законодательством для приема денежных средств используются специальные компьютерные системы.
                </span>
            </>)
        }, {
            title: 'Какой банк-эквайер выбрать?',
            description: (<>
                <span className={cls.faq_text_block}>
                    zPOS может работать с одним из нескольких банков-эквайеров на Ваш выбор (уточняйте в разделе описания конкретной модели).
                </span>
                <span className={cls.faq_text_block}>
                    Чаще всего банк-эквайер удобно выбирать по принципу «Одно окно», когда ваш расчетный счет находится в этом же банке, или минимальной комиссии за эквайринг. С выбранным банком-эквайером необходимо заключить договор эквайринга.
                </span>
            </>)
        }, {
            title: 'Какие условия гарантии и где сервисный центр?',
            description: (<>
                <span className={cls.faq_text_block}>
                    На приобретенный zPOS.Терминал устанавливается гарантийный срок, который предусматривает бесплатный ремонт или замену (в случае неремонтопригодности) в течение 12 месяцев, при условии соблюдения правил эксплуатации.
                </span>
                <span className={cls.faq_text_block}>
                    Дополнительно Вы можете приобрести пакет сервисного обслуживания, для минимизации простоя Вашего бизнеса. В рамках сервисного обслуживания вы получаете:
                </span>
                <ol>
                    <li>технические консультации;</li>
                    <li>ремонт (оплата запасных частей только в случае негарантийного ремонта);</li>
                    <li>предоставление подменного терминала на время ремонта;</li>
                    <li>доставку и возврат оборудования для выполнения ремонта нашими силами.</li>
                </ol>

                <span className={cls.faq_text_block}>
                    <strong>Сервисный центр находится по адресу:</strong> Минск, Партизанский пр-т, 178.
                </span>
                <span className={cls.faq_text_block}>
                    <strong>Единая платная линия технической поддержки:</strong> 7040 (для абонентов А1, МТС).
                </span>
                <span className={cls.faq_text_block}>
                    <strong>Режим работы:</strong> с 9:30 до 17:30 (пт до 16:15), выходные дни: сб, вс.
                </span>
            </>)
        },
    ]
    const [expanded, setExpanded] = useState([]);
    const handleChange = title => {
        let a = [...expanded]
        if (!a.filter(el => el === title).length) {
            a.push(title)
        } else {
            a = a.filter(el => el !== title)
        }
        setExpanded(a)
    };
    return (
        <div className={cls.faq_container}>
            <Container style={{ position: 'relative' }} component="main" maxWidth="lg">
                <div className={cls.faq_block_card}>
                    <h1><span>Вопрос/ответ</span></h1>
                    <div className={cls.faq_block_faq_wrapper}>
                        <div className={cls.faq_block_img}>
                            <img src={block1} alt='1' />
                        </div>
                        <div className={cls.faq_block_faq_panel_summary}>
                            {FaqArray.map(({ title, description }, index) => (<>
                                <div onClick={() => handleChange(title)} className={cls.faq_title_style}>
                                    <div className={cls.faq_title_style_cur}>{title}</div>
                                    <CollapseButton onClick={() => handleChange(title)} boolean={!Boolean(expanded.filter(el => el === title).length)} />
                                </div>
                                <Collapse in={Boolean(expanded.filter(el => el === title).length)} timeout="auto" unmountOnExit>
                                    <div className={cls.faq_block_description}>{description}</div>
                                </Collapse>
                            </>
                            ))}
                        </div>
                    </div>
                </div>
            </Container>
        </div>)
};

export default Faq;