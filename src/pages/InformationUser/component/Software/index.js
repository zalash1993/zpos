import React from 'react';
import cls from './software.module.scss';
import { Container } from '@material-ui/core';
import Slider from 'react-slick';
import arrow from '../../../../img/arrow.svg';
import arrow2 from '../../../../img/arrow2.svg';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import block1 from '../../../../img/img_software/block1.png';
import block2 from '../../../../img/img_software/block2.png';
import block3 from '../../../../img/img_software/block3.png';
import block4 from '../../../../img/img_software/block4.png';
import block5 from '../../../../img/img_software/block5.png';
import block6 from '../../../../img/img_software/block6.png';
import block7 from '../../../../img/img_software/block7.png';

const Software = () => {
    const NextArrow = ({ onClick }) => (
        <img
            onClick={onClick}
            className={"slick-arrow slick-next"}
            src={arrow2}
            alt="nextArrow" />
    );
    const PrevArrow = ({ onClick }) => (
        <img
            onClick={onClick}
            className={"slick-arrow slick-prev"}
            src={arrow}
            alt="prevArrow" />
    );

    let settings = {
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 7000,
        speed: 700,
        arrows: true,
        autoplay: true,
        fade: true,
        dots:true,
        prevArrow: <PrevArrow />,
        nextArrow: <NextArrow />
    };
    return (
        <div className={cls.software_container}>

            <div className={cls.software_block_card}>
                <h1>Программная касса - zPOS.Касса</h1>
                <div className={cls.software_slider_block_wrapper}>
                    <Slider {...settings}>
                        <Container style={{ position: 'relative' }} component="main" maxWidth="lg">
                            <div className={cls.software_slide}>
                                <div className={`${cls.software_img_container} ${cls.software_img_container_wrapper}`}>
                                    <div className={cls.software_current_img}>
                                        <img src={block1} alt='1' />
                                    </div>
                                    <div className={cls.software_current_img}>
                                        <img src={block2} alt='2' />
                                    </div>
                                    <div className={cls.software_current_img}>
                                        <img src={block3} alt='3' />
                                    </div>
                                </div>
                                <div className={cls.software_text_container}>
                                    <div className={cls.software_text_img}>
                                        <img src={block4} alt='3' />
                                    </div>
                                    <div className={cls.software_current_text}>
                                        <span>Разработано в соответствии с требованиями:</span>
                                        <span>Постановления Совета министров Республики Беларусь и</span>
                                        <span>Национального банка Республики Беларусь от</span>
                                        <span>29 декабря 2017 г. № 1040/17,</span>
                                        <span>Постановления Министерства по налогам и сборам</span>
                                        <span>Республики Беларусь от 29 марта 2018 г. № 10.</span>
                                    </div>
                                </div>
                            </div>
                        </Container>
                        <Container style={{ position: 'relative' }} component="main" maxWidth="lg">
                            <div className={cls.software_slide}>
                                <div className={cls.software_img_container_wrapper}>
                                    <div className={cls.top_text}>Программное обеспечение "zPOS.Касса" работает на zPOS.Терминале и обеспечивает:</div>
                                    <div className={cls.software_img_container}>
                                        <div className={cls.software_current_img}>
                                            <img src={block5} alt='1' />
                                        </div>
                                        <div className={cls.software_current_img}>
                                            <img src={block6} alt='2' />
                                        </div>
                                        <div className={cls.software_current_img}>
                                            <img src={block7} alt='3' />
                                        </div>
                                    </div>
                                </div>
                                <div className={cls.software_text_container}>
                                    <div className={cls.software_text_img}>
                                        <img src={block4} alt='3' />
                                    </div>
                                    <div className={cls.software_current_text}>
                                        <span>Абонентская плата за использование</span>
                                        <span>программного обеспечения составляет:</span>
                                        <h2>от 4.1 руб./мес. без НДС*</h2>
                                    </div>
                                </div>
                            </div>
                            <div className={cls.line_text}>
                                * На основании п.27 Положения о Парке высоких технологий, утв. Декретом Президента Республики Беларусь от 22.09.2005 №12 (в ред. Декрета Президента Республики Беларусь от 21.12.2017 №8)
                                </div>
                        </Container>
                    </Slider>
                </div>
            </div>

        </div>)
};

export default Software;