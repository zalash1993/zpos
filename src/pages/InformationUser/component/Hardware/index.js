import React, { useState } from 'react';
import cls from './hardware.module.scss';
import { Container } from '@material-ui/core';
import Slider from 'react-slick';
import block1 from '../../../../img/img_hardware/block1.png';
import block2 from '../../../../img/img_hardware/block2.png';
import block3 from '../../../../img/img_hardware/block3.png';
import block4 from '../../../../img/img_hardware/block4.png';
import block5 from '../../../../img/img_hardware/block5.png';
import block6 from '../../../../img/img_hardware/block6.png';
import block7 from '../../../../img/img_hardware/block7.png';
import block8 from '../../../../img/img_hardware/block8.png';
import arrow from '../../../../img/arrow.svg';
import arrow2 from '../../../../img/arrow2.svg';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ButtonStyle from '../../../../components/ButtonStyle';
import { Link } from 'react-router-dom';
import arrJson from './img.json';


const Hardware = () => {
    const NextArrow = ({ onClick }) => (
        <img
            onClick={onClick}
            className={"slick-arrow slick-next"}
            src={arrow2}
            alt="nextArrow" />
    );
    const PrevArrow = ({ onClick }) => (
        <img
            onClick={onClick}
            className={"slick-arrow slick-prev"}
            src={arrow}
            alt="prevArrow" />
    );

    let settings = {
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplaySpeed: 7000,
        arrows: true,
        autoplay: true,
        prevArrow: <PrevArrow />,
        nextArrow: <NextArrow />
    };
    const imgArrDesc = {
        block1,
        block2,
        block3,
        block4,
        block5,
        block6,
        block7,
        block8,
    }
    const buttonTitleFunc = (indSlider, indSlide) => arrJson[indSlider].img[indSlide];
    const [buttonTitle, setButtonTitle] = useState({ 0: buttonTitleFunc(0, 0), 1: buttonTitleFunc(1, 0), 2: buttonTitleFunc(2, 0) });
    return (
        <div className={cls.hardware_container}>
            <Container style={{ position: 'relative' }} component="main" maxWidth="lg">
                <div className={cls.hardware_block_card}>
                    <h1><span>Оборудование - zPOS.Терминалы</span></h1>
                    <div className={cls.hardware_slider_block_wrapper}>
                        {arrJson.map(({ title1, title2, img }, index) => <div key={title1} className={cls.hardware_current_block}>
                            <div className={cls.hardware_slider_block_title}>
                                <span>{title1}</span>
                                <span>{title2}</span>
                            </div>
                            <Slider beforeChange={(indexSlide, nextIndex) => {
                                setButtonTitle({ ...buttonTitle, [index]: buttonTitleFunc(index, nextIndex) });
                            }} {...settings}>
                                {img.map(({ link, title, price, src }) => <div key={title} className={cls.hardware_slide}>
                                    <div className={cls.hardware_title_slide}><span>{title}</span></div>
                                    <div className={cls.hardware_img_container_wrapper}>
                                        <Link to={link} target="_blank">
                                            <img src={imgArrDesc[src]} className={cls.hardware_img_wrapper_banner} alt={title} />
                                        </Link>
                                    </div>
                                    <div className={cls.hardware_price}>{price}</div>
                                </div>)}
                            </Slider>
                            <div className={cls.hardware_button_block}>
                                {buttonTitle[index].button && <div className={cls.button_wrapper}><ButtonStyle onClick={() => alert(buttonTitle[index].title)}>{buttonTitle[index].button}</ButtonStyle></div>}
                                <div className={`${cls.button_wrapper} ${!buttonTitle[index].button ? cls.button_one_wrapper : ''}`}>
                                    <ButtonStyle styleVariable='outlined'>Подробнее</ButtonStyle>
                                </div>
                            </div>
                        </div>)}
                    </div>
                </div>
            </Container>
        </div>)
};

export default Hardware;