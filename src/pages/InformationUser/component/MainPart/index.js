import React, { useState } from 'react';
import cls from './mainPart.module.scss';
import { Container, Tooltip } from '@material-ui/core';
import block1 from '../../../../img/img_main_part/block1.png';
import block2 from '../../../../img/img_main_part/block2.png';
import block3 from '../../../../img/img_main_part/block3.png';
import block4 from '../../../../img/img_main_part/block4.png';
import block5 from '../../../../img/img_main_part/block5.png';
import block6 from '../../../../img/img_main_part/block6.png';
import block7 from '../../../../img/img_main_part/block7.png';
import block8 from '../../../../img/img_main_part/block8.png';
import block9 from '../../../../img/img_main_part/block9.png';
import { Link } from 'react-router-dom';
import arrJson from './img.json'

const MainPart = () => {
    const [silverStyle, setSilverStyle] = useState(false);
    const imgArrDesc = {
        block1,
        block2,
        block3,
        block4,
        block5,
        block6,
        block7,
        block8,
        block9
    }
    return (
        <div className={cls.mainPart_container}>
            <Container style={{ position: 'relative' }} component="main" maxWidth="lg">
                <div className={cls.mainPart_block_card}>
                    <h1>zPOS - программная касса и всё для неё</h1>
                    <div className={`${cls.mainPart_block_img_wrapper} ${cls.mainPart_desktop} ${silverStyle ? cls.mainPart_block_img_wrapper_silver : ''}`}>
                        {arrJson.map(({ img }, index) => <div key={index} className={cls.mainPart_block_img}>{img.map(({ link, title, description, src }) => (
                            <Link key={src} to={link} target="_blank">
                                <Tooltip arrow placement="bottom" title={<><div>{title}</div><div>{description}</div></>} >
                                    <img
                                        onMouseOver={() => setSilverStyle(true)}
                                        onMouseOut={() => setSilverStyle(false)}
                                        src={imgArrDesc[src]}
                                        alt={title}
                                    />
                                </Tooltip>
                            </Link>
                        ))}</div>)}
                    </div>
                    <div className={`${cls.mainPart_block_img_wrapper} ${cls.mainPart_mobile} ${silverStyle ? cls.mainPart_block_img_wrapper_silver : ''}`}>
                        {arrJson.map(({ img }, index) => img[0].src_mob ? <div key={index} className={cls.mainPart_block_img}>{img.map(({ link, title, description, src_mob }) => (
                            <Link key={src_mob} to={link}>
                                    <img
                                        onMouseOver={() => setSilverStyle(true)}
                                        onMouseOut={() => setSilverStyle(false)}
                                        src={imgArrDesc[src_mob]}
                                        alt={title}
                                    />
                            </Link>
                        ))}</div> : null)}
                    </div>
                </div>
            </Container>
        </div>)
};

export default MainPart;