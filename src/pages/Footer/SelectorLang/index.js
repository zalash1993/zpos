import React, { useState } from 'react';
import { FormControl, Select, MenuItem } from '@material-ui/core';
import i18n from 'i18next';
import Language from '../../../helpers/language';
import cls from './style.module.scss';

const SelectorLang = () => {
    const [local, setLocal] = useState(Language());
    const handleChange = ({target:{value}}) => {
        setLocal(value);
        localStorage.setItem('language', value);
        i18n.changeLanguage(value);
    };

    return (
        <FormControl variant="outlined" >
            <Select
                value={local}
                onChange={handleChange}
                className={cls.style_select}
            >
                <MenuItem value='ru'>RU</MenuItem>
                <MenuItem value='en'>EN</MenuItem>
            </Select>
        </FormControl>
    )
}
export default SelectorLang;