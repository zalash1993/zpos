import React from 'react';
import cls from './footer.module.scss';
import SelectorLang from './SelectorLang';
import { useTranslation } from 'react-i18next';


const Footer = () => {
    const { t } = useTranslation();
    const date = (new Date().getYear()+1900);
return (<footer className={cls.footer_container}>
    {t('footer.title1')} {date}{t('footer.title2')} <SelectorLang/>
</footer>)
}

export default Footer;