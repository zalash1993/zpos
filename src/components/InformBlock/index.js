import React from 'react';
import cls from './inform.module.scss'
import { useSelector } from 'react-redux';
import { Card } from '@material-ui/core';
import { useLocation, 
    // Link 
} from 'react-router-dom';
// import SettingsIcon from '@material-ui/icons/Settings';
import { useTranslation } from 'react-i18next';

const InformBlock = () => {
    const { t } = useTranslation();
    const { pathname } = useLocation();
    const info = useSelector((state) => state.authentication.user);
    return (
        <Card className={cls.info_container_wrapper}>
            {pathname !== '/' ?
                <>
                    <div className={cls.info_item_content}>{t('InformBlock.balance')}: <span>{info.accBalance ? info.accBalance.toFixed(2) : '0.00'}</span> {t('rub')}</div>
                    <div className={cls.info_item_content}>
                        <li className={cls.org_short_name}> {info?.orgShortName}</li>
                        <div className={cls.info_item_color}> {info?.personName}</div>
                    </div>
                </> :
                <div className={cls.info_item_content_home}>
                    <div> {info.personName ? `${t('InformBlock.goodAfternoon')}, ${info?.personName}!` : `${t('InformBlock.goodAfternoon')}!`}</div>
                </div>}
            {/* <Link to='/settings' title={t('InformBlock.settings')} className={cls.info_settings_link}>
                <SettingsIcon fontSize="small" className={cls.info_settings} />
            </Link> */}
        </Card>
    )
}
export default InformBlock;