import React from 'react';
import cls from './style.module.scss';
import {
    Toolbar,
    Typography,
    Tooltip,
    IconButton,
} from '@material-ui/core';
import FilterListIcon from '@material-ui/icons/FilterList';
import statusIconGenerator from '../../helpers/statusIconGenerator';
import { useTranslation } from 'react-i18next';


const EnhancedTableToolbar = ({ setFilterOpen, setFilterActive, filterActive }) => {
    const { t } = useTranslation();
    return (
        <>
            <Toolbar>
                <Typography className={cls.orders_expansion_table_title} variant="h6" id="tableTitle">
                    <span>{t('pages.orders.title')}</span>
                </Typography>
                <Tooltip title={t('filter')} onClick={setFilterOpen} className={cls.equipment_and_services_filter_button}>
                    <IconButton aria-label={t('filter')}>
                        <FilterListIcon />
                    </IconButton>
                </Tooltip>
            </Toolbar>
            <div className={cls.block_filter_all_wrapper}>
                {Object.keys(filterActive).map((el, index) => {
                    let a = filterActive[el]
                    if (el === 'orderStatus') {
                        a = statusIconGenerator(a, 'doubleIcon', t)
                    }
                    if (el === 'flagAuto') {
                        if (a > 0) {
                            a = t('tableRow.yes');
                        } else {
                            a = t('tableRow.no');
                        }
                    }
                    return (
                        <div key={index} className={cls.current_block_filter_wrapper} title={filterActive[el]} onClick={() => {
                            const newFilterActive = { ...filterActive };
                            delete newFilterActive[el];
                            setFilterActive(newFilterActive)
                        }}>
                            <div className={`${cls.current_block_filter} ${el === 'orderStatus' ? cls.current_block_filter_top : ''}`}>
                                {a}
                            </div>
                        </div>)
                })}
            </div>
        </>
    );
};

export default EnhancedTableToolbar;