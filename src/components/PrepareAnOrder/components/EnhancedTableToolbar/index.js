
import React from 'react';
import {
    Toolbar,
    Typography
} from '@material-ui/core';
import cls from './style.module.scss';
import { useTranslation } from 'react-i18next';

const EnhancedTableToolbar = () => {
    const { t } = useTranslation();
    return (
        <Toolbar>
            <Typography className={cls.prepare_order_title} variant="h6" id="tableTitle">
                {t('general.titlePrepare')}
        </Typography>
        </Toolbar>
    );
};

export default EnhancedTableToolbar;