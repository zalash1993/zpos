import React, { useState, useEffect, useRef } from 'react';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Paper,
    Switch,
} from '@material-ui/core';
import moment from 'moment';
import DataPickerComponent from '../DataPickerComponent';
import cls from './prepareOrder.module.scss'
import services from '../../redux/actions/services';
import { useDispatch } from 'react-redux';
import EnhancedTableToolbar from './components/EnhancedTableToolbar';
import Loading from '../Loading';
import { useTranslation } from 'react-i18next';

const PrepareAnOrder = ({ rows, prepareAnOrder, flagAuto, loading }) => {
    const { t } = useTranslation();
    const [selected, setSelected] = useState([]);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [checked, setChecked] = useState(!!flagAuto);
    const [dateMount, setDateMount] = useState(false);
    const handleClick = (event, name) => {
        if (!name.active) {
            return;
        }
        if (selected[0] === name) {
            return;
        }
        setSelected([name]);
    };
    useEffect(() => {
        if (selected.length) {
            const newBeginDate = selected.length ? new Date(selected[0].beginDate.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1')) : new Date();
            const newEndDate = selected.length ? new Date(selected[0].endDate.replace(/(\d+).(\d+).(\d+)/, '$3/$2/$1')) : new Date();
            setStartDate(newBeginDate);
            setEndDate(newEndDate);
            goToDateBlock();
            setDateMount(true);
        } else {
            setDateMount(false);
        }
    }, [selected]);
    useEffect(() => {
        const newStartDate = new Date(startDate);
        const newDate = selected.length ? newStartDate.setMonth(newStartDate.getMonth() + selected[0].mmcnt) : startDate;
        setEndDate(new Date(newDate));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [startDate, selected]);
    const isSelected = name => selected.indexOf(name) !== -1;

    const refDateBlock = useRef();
    const goToDateBlock = () => {
        if (refDateBlock.current) {
            refDateBlock.current.scrollIntoView({ behavior: 'smooth' });
        }
    };
    const dispatch = useDispatch();
    useEffect(() => {
        if (selected.length && dateMount) {
            const params = {
                orderParams: {
                    beginDate: moment(startDate).format('DD.MM.YYYY'),
                    endDate: moment(endDate).format('DD.MM.YYYY'),
                    flagAuto: checked ? 1 : 0,
                    termID: selected[0].termID
                }
            };
            prepareAnOrder(params);
        } else {
            dispatch(services.resetOrderPay());
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [startDate, selected, checked]);

    return (
        <div className={cls.prepare_order_container}>
            <Loading load={loading} />
            <Paper className={cls.prepare_order_paper}>
                <EnhancedTableToolbar />
                <TableContainer>
                    <Table
                        className={cls.prepare_order_table}
                        aria-labelledby="tableTitle"
                        size='medium'
                        aria-label="enhanced table"
                    >
                        <TableBody className={cls.mobile_version_table_border}>
                            {rows.map((row, index) => {
                                const isItemSelected = isSelected(row);
                                return (
                                    <TableRow
                                        onClick={event => handleClick(event, row)}
                                        tabIndex={-1}
                                        key={index}
                                        colSpan={7}
                                        className={isItemSelected ? cls.table_row_disable : ''}
                                    >
                                        <TableCell className={`${cls.desktop_version_table} ${!row.active ?cls.desktop_version_table_error:''}`}>
                                            {`${row.termName} - ${row.price_perMM.toFixed(2)} ${t('rub')} / ${t('month')}`}
                                        </TableCell>
                                        <TableCell className={`${cls.desktop_version_table} ${!row.active ?cls.desktop_version_table_error:''}`} align="right">{row.errTerm ?
                                            <span className={cls.table_error}>{t('general.notAvailable')}</span> :
                                            row.errBalance ?
                                                <span className={cls.table_error}>{t('general.insufficientFunds')}</span> : ''}
                                            {`${row.price_total.toFixed(2)} ${t('rub')}`}
                                        </TableCell>
                                        <TableCell className={cls.mobile_version_table}>
                                            <div>{`${row.termName} - ${row.price_perMM.toFixed(2)} ${t('rub')} / ${t('month')}`}</div>
                                            {row.errTerm ?
                                                <span className={cls.table_error}>{t('general.notAvailable')}</span> :
                                                row.errBalance ?
                                                    <span className={cls.table_error}>{t('general.insufficientFunds')}</span> : ''}
                                            <div>{`${row.price_total.toFixed(2)} ${t('rub')}`}</div>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                {selected.length > 0 && <div>
                    <div className={cls.prepare_order_date_block}>
                    {t('general.enableWith')} <DataPickerComponent minDate={new Date()} selected={startDate} onChange={setStartDate} /> {t('general.by')} {moment(endDate).format('DD.MM.YYYY')}
                    </div>
                    <div>
                        <Switch
                            checked={checked}
                            onChange={() => setChecked(!checked)}
                            className={cls.switch_style_active}
                        />
                        {t('general.autoRepeat')}
                </div>
                </div>}
            </Paper>
        </div>
    );
};

export default PrepareAnOrder;