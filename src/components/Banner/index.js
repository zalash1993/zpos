/* eslint-disable react/jsx-no-target-blank */
import React from 'react';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
// import belgaz from '../../img/belgaz.png';
// import datecs from '../../img/datecs.png';
// import mtbank from '../../img/mtbank.png';
// import newpos from '../../img/newpos.png';
// import velcom from '../../img/velcom.png';
// import dockStation from '../../img/dockStation.png';
// import banner from '../../img/banner.jpeg';
// import banner1 from '../../img/banner1.jpg';
// import banner2 from '../../img/banner2.jpg';
import banner3 from '../../img/banner3.jpg';
import banner4 from '../../img/banner4.jpg';
import cls from './banner.module.scss'
// import { useTranslation } from 'react-i18next';

const Banner = () => {
    // const { t } = useTranslation();
    let settings = {
        className: "slider variable-width",
        infinite: true,
        speed: 4000,
        slidesToShow: 1,
        slidesToScroll: 1,
        // lazyLoad: true,
        autoplaySpeed: 10000,
        autoplay: true,
        // cssEase: "linear",
        dots: true,
        // arrows: false
        // variableWidth: true
    };
    return (<div className={cls.banner_container}>
        <div className={cls.banner_container_wrapper}>
            <Slider {...settings}>
                <div>
                    <a className={cls.container_wrapper} href="https://zpos.by/dockstation" target="_blank">
                        <img src={banner3} className={cls.img_wrapper_banner} alt='banner' />
                    </a>
                </div>
                <div>
                    <a className={cls.container_wrapper} href="https://zpos.by/#service" target="_blank">
                        <img src={banner4} className={cls.img_wrapper_banner} alt='banner' />
                    </a>
                </div>
            </Slider>
        </div>
    </div>
    )
}
export default Banner;