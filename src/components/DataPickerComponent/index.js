import React, { forwardRef } from 'react';
import ReactDatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import PropTypes from 'prop-types';
import moment from 'moment';
import 'moment/locale/ru';
import 'moment/locale/en-au';
import DateRangeOutlinedIcon from '@material-ui/icons/DateRangeOutlined';
import ButtonStyle from '../ButtonStyle';
import cls from './dataPicker.module.scss';
import ru from 'date-fns/locale/ru';
import { useTranslation } from 'react-i18next';

const DataPickerComponent = ({ selected, onChange, minDate, showMonthYearPicker, month, maxDate }) => {
    const { i18n: { language } } = useTranslation();
    const ExampleCustomInput = forwardRef(({ value, onClick }, ref) => {
        return (
            <ButtonStyle
                innerRef={ref}
                endIcon={<DateRangeOutlinedIcon fontSize="small" />}
                forwardRef={ref}
                styleVariable='outlined'
                onClick={onClick}>
                {moment(new Date(value)).locale(language).format(month ? 'MMMM' : 'DD.MM.YYYY', 'ru')}
            </ButtonStyle>
        )
    });
    return (
        <div className={cls.data_picker_wrapper}>
            <ReactDatePicker
                selected={selected}
                onChange={onChange}
                customInput={<ExampleCustomInput />}
                minDate={minDate}
                maxDate={maxDate}
                withPortal
                locale={language === 'ru' ? ru : ''}
                showMonthYearPicker={showMonthYearPicker}
            />
        </div>
    )
}
DataPickerComponent.propTypes = {
    selected: PropTypes.instanceOf(Date),
    onChange: PropTypes.func
};

DataPickerComponent.defaultProps = {
    selected: new Date(),
    minDate: new Date(),
    maxDate: null,
    showMonthYearPicker: false,
    month: false
};
export default DataPickerComponent;