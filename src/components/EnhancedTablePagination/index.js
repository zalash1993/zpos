import React from 'react';
import cls from './style.module.scss';
import {
    TablePagination,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';


const EnhancedTablePagination = ({
    count,
    rowsPerPage,
    page,
    onChangePage,
    onChangeRowsPerPage,
    className
}) => {
    const { t } = useTranslation();
    return (
        <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={count}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={onChangePage}
            onChangeRowsPerPage={onChangeRowsPerPage}
            labelRowsPerPage={t('pagination.title')}
            labelDisplayedRows={
                ({ from, to, count }) => {
                    return `${from} - ${to} ${t('general.of')} ${count}`
                }
            }
            className={`${cls.equipment_and_services_pagination} ${className ? className : ''}`}
        />
    );
};

export default EnhancedTablePagination;