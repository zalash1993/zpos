import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import TableRowDesktop from './components/TableRowDesktop';
import TableRowMobile from './components/TableRowMobile';

const HandlerRowDesktopAndMobile = ({ paddingCheck,checkboxnone, cursor, handleClickName, handleClickCheckBox, checkbox, headCells, handleClick, row, isItemSelected, index, style, column, minimize, className }) => {
    return (
        <Fragment>
            <TableRowDesktop
                headCells={headCells}
                row={row}
                checkbox={checkbox}
                index={index}
                handleClick={handleClick}
                isItemSelected={isItemSelected}
                style={style}
                cursor={cursor}
                className={className}
                handleClickCheckBox={handleClickCheckBox}
                handleClickName={handleClickName}
                checkboxnone={checkboxnone}
                paddingCheck={paddingCheck}
            />
            <TableRowMobile
                headCells={headCells}
                row={row}
                column={column}
                checkbox={checkbox}
                index={index}
                handleClick={handleClick}
                isItemSelected={isItemSelected}
                style={style}
                minimize={minimize}
                className={className}
                checkboxnone={checkboxnone}
            />
        </Fragment>
    )
}

HandlerRowDesktopAndMobile.propTypes = {
    checkbox: PropTypes.bool,
    column: PropTypes.bool,
    style: PropTypes.shape({}),
    handleClick: PropTypes.func,
    headCells: PropTypes.array.isRequired,
    row: PropTypes.shape({}).isRequired,
    index: PropTypes.number,
    isItemSelected: PropTypes.bool,
    minimize: PropTypes.bool,
    cursor: PropTypes.bool
};

HandlerRowDesktopAndMobile.defaultProps = {
    checkbox: false,
    column: false,
    cursor: false,
    style: {},
    handleClick: () => { },
    handleClickCheckBox: () => { },
    handleClickName: () => { },
    isItemSelected: false,
    minimize: false,
    className: '',
    checkboxnone: false,
    paddingCheck:false
};

export default HandlerRowDesktopAndMobile;