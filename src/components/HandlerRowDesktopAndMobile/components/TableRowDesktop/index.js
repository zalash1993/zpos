import React from 'react';
import { TableRow, TableCell } from '@material-ui/core';
import cls from './tableRowDesktop.module.scss';
import CheckboxStyle from '../../../CheckboxStyle';
import statusIconGenerator from '../../../../helpers/statusIconGenerator';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

const TableRowDesktop = ({
    paddingCheck,
    checkboxnone,
    cursor,
    handleClickName,
    checkbox,
    headCells,
    handleClick,
    handleClickCheckBox,
    row,
    isItemSelected,
    index,
    style,
    className
}) => {
    const { t } = useTranslation();
    return (<TableRow
        style={style}
        onClick={event => handleClick(event, row)}
        tabIndex={-1}
        key={index}
        className={`${cls.desktop_version_table} ${cursor ? cls.desktop_version_table_cursor : ''} ${isItemSelected ? cls.desktop_version_table_selected : ''}`}
    >
        {checkbox && <TableCell padding="checkbox" style={paddingCheck ? { textAlign: 'center' } : null}>
            <CheckboxStyle
                style={paddingCheck ? { padding: 2 } : null}
                onChange={event => handleClickCheckBox(event, row)}
                checked={isItemSelected}
            />
        </TableCell>}
        {checkboxnone && <TableCell padding="checkbox">
            <div style={{ height: 40, display: 'flex', justifyContent: 'center' }}>{statusIconGenerator(row.servStatus, "icon", t)}</div>
        </TableCell>}

        {headCells.map((el, index) => {
            let rowValue = row[el.id];
            if (el.icon) {
                rowValue = statusIconGenerator(row[el.id], null, t);
            };
            if (el.doubleIcon) {
                rowValue = statusIconGenerator(row[el.id], 'doubleIcon', t);
            };
            if (el.bool) {
                rowValue === 0 ? rowValue = t("tableRow.no") : rowValue = t("tableRow.yes");
            };
            if (el.orderNum) {
                if (Boolean(row.operType)) {
                    rowValue = <Link className={cls.link_style} to={`/orders?orderNum=${row.orderNum}`}>{t("tableRow.orderNum.true")}{row.orderNum}</Link>;
                } else {
                    rowValue = t("tableRow.orderNum.false");
                };
            };
            if (el.iconImg) {
                if (Boolean(row.operType)) {
                    rowValue = <div className={cls.block_icon_img}><RemoveCircleOutlineIcon className={cls.minus_icon} />{rowValue}</div>
                } else {
                    rowValue = <div className={cls.block_icon_img}><AddCircleOutlineIcon className={cls.plus_icon} />{rowValue}</div>
                };
            };
            if (el.prefix) {
                rowValue = `${rowValue.toFixed(2)} ${el.prefix}`;
            };
            return (
                <TableCell
                    onClick={el.id === 'name' ? handleClickName : null}
                    className={className} key={index}
                    align={el.align}>
                    {rowValue ? rowValue : '-'}
                </TableCell>)
        })}
    </TableRow>)
}

export default TableRowDesktop;