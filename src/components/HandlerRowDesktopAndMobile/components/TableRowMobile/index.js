import React, { useEffect, useState } from 'react';
import { TableRow, TableCell } from '@material-ui/core';
import cls from './tableRowMobile.module.scss';
import CheckboxStyle from '../../../CheckboxStyle';
import statusIconGenerator from '../../../../helpers/statusIconGenerator';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

const TableRowMobile = ({
    checkboxnone,
    checkbox,
    headCells,
    handleClick,
    row,
    isItemSelected,
    index,
    style,
    column,
    minimize,
    className
}) => {
    const { t } = useTranslation();
    const [newHeadCells, setNewHeadCells] = useState(headCells);
    const swap = (a, b) => {
        const copy = [...newHeadCells];
        copy[a] = copy.splice(b, 1, copy[a])[0];
        setNewHeadCells(copy)
    }
    useEffect(() => {
        if (headCells.find(el => el.id === "servStatus")) {
            swap(0, 1)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (<TableRow
        style={style}
        onClick={event => handleClick && handleClick(event, row)}
        className={`${cls.mobile_version_table} ${isItemSelected ? cls.mobile_version_table_selected : ''}`}
    >
        <TableCell colSpan={7} style={Object.keys(style).length ? { borderBottom: 'none' } : {}} className={cls.mobile_card_wrapper}>
            {newHeadCells.map((el, index) => {
                let rowValue = row[el.id];
                if (el.icon) {
                    rowValue = statusIconGenerator(row[el.id], null, t);
                };
                if (el.doubleIcon) {
                    rowValue = statusIconGenerator(row[el.id], 'doubleIcon', t);
                };
                if (el.bool) {
                    rowValue === 0 ? rowValue = t("tableRow.no") : rowValue = t("tableRow.yes");
                };
                if (el.orderNum) {
                    if (Boolean(row.operType)) {
                        rowValue = <Link className={cls.link_style} to={`/orders?orderNum=${row.orderNum}`}>{t("tableRow.orderNum.true")}{row.orderNum}</Link>;
                    } else {
                        rowValue = t("tableRow.orderNum.false");
                    };
                };
                if (el.iconImg) {
                    if (Boolean(row.operType)) {
                        rowValue = <div className={cls.block_icon_img}><RemoveCircleOutlineIcon className={cls.minus_icon} />{rowValue}</div>
                    } else {
                        rowValue = <div className={cls.block_icon_img}><AddCircleOutlineIcon className={cls.plus_icon} />{rowValue}</div>
                    };
                };

                if (el.prefix) {
                    rowValue = `${rowValue.toFixed(2)} ${el.prefix}`;
                };
                return (<div key={index} className={`${cls.mobile_card_row} ${(minimize && index === 0) || el.mobileLine ? cls.mobile_card_row_inline : ''} ${column ? cls.mobile_card_row_column : ''}`}>
                    {minimize && index === 0 ? '' : <div className={`${cls.mobile_card_name} ${className}`}>{el.label}{' : '}</div>}
                    {checkbox && minimize && index === 0 && <CheckboxStyle
                        checked={isItemSelected}
                    />}
                    {checkboxnone && minimize && index === 0 &&
                        <div style={{ height: 40, display: 'flex', justifyContent: 'center' }}>{statusIconGenerator(row.servStatus, "icon", t)}</div>
                    }
                    <div className={`${className} ${minimize && index === 0 ? cls.mobile_card_row_minimize_text : ''}`}>{rowValue ? rowValue : '-'}</div>
                </div>)
            })}
        </TableCell>
    </TableRow>)
}

export default TableRowMobile;