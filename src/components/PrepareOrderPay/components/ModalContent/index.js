import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import cls from './modalContent.module.scss'
import { useTranslation } from 'react-i18next';

const ModalContent = ({ close }) => {
    const { t } = useTranslation();
    const modalContent = useSelector((state) => state.services.orderPay);
    return (
        <div className={cls.modal_content_wrapper}>
            <div className={cls.modal_content_head}>
                <h2>{t('modal.thanks')}</h2>
                <h2>{t('modal.orderHasBeen')}</h2>
            </div>
            <div className={cls.modal_content_inform}>{t('modal.information')}</div>
            <div className={cls.modal_content_main}>
                <span>{t('modal.orderNo')}{modalContent?.orderNum}</span>
            </div>
            <div className={cls.modal_content_link}>
                <Link onClick={close} to='/orders'>{t('modal.goTo')}</Link>
            </div>
        </div>
    )
}

ModalContent.defaultProps = {
    close: () => { }
}

export default ModalContent;