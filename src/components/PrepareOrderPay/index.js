import React, { useState, Fragment } from 'react';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow,
    Toolbar,
    Typography,
    Paper,
    Switch,
} from '@material-ui/core';
import Modal from '../Modal';
import ModalContent from './components/ModalContent';
import ModalContentWarning from './components/ModalContentWarning';
import cls from './prepareOrderPay.module.scss';
import EnhancedTableHead from './components/EnhancedTableHead';
import HandlerRowDesktopAndMobile from '../HandlerRowDesktopAndMobile';
import Loading from '../Loading';
import ButtonStyle from '../ButtonStyle';
import HintComponent from '../HintComponent';
import ErrorIcon from '@material-ui/icons/Error';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';

const EnhancedTableToolbar = ({ params, t }) => {
    return (
        <Toolbar
            className={cls.prepare_order_pay_table_toolbar}
        >
            <Typography className={cls.prepare_order_pay_table_title} variant="h6" id="tableTitle">
                <div>{`${t('general.orderValidityPeriod')} ${params.beginDate} ${t('general.by2')} ${params.endDate}`}</div>
            </Typography>
        </Toolbar>
    );
};

const PrepareOrderPay = ({ rows, prepareAnOrder, params, resetOrder, warning, error, loading, backStep }) => {
    const { t } = useTranslation();
    const headCells = [
        { id: 'equType', numeric: false, disablePadding: true, align: 'left', label: t('headCells.equType'), bool: false, mobileLine: false },
        { id: 'serialNumber', numeric: true, disablePadding: false, align: 'left', label: t('headCells.serialNumber'), bool: false, mobileLine: true },
        { id: 'servName', numeric: true, disablePadding: false, align: 'left', label: t('headCells.servName'), bool: false, mobileLine: false },
        { id: 'price_perMM', numeric: true, disablePadding: false, align: 'right', label: t('headCells.price_perMM'), bool: false, mobileLine: true, prefix: t('rub') },
        { id: 'termName', numeric: true, disablePadding: false, align: 'left', label: t('headCells.termName'), bool: false, mobileLine: true },
    ];
    const [openModal, setOpenModal] = useState(false);
    const [openModalWarning, setOpenModalWarning] = useState(false);
    const paymentClickHandler = () => {
        setOpenModalWarning(false);
        const paramRec = {
            orderParams: params
        }
        prepareAnOrder(paramRec, () => setOpenModal(true))
    }
    return (
        <Paper className={cls.prepare_order_pay_paper_wrapper}>

            <Loading load={loading} />
            <EnhancedTableToolbar params={params} t={t} />
            <TableContainer style={{ overflowX: 'visible' }}>
                <Table
                    className={cls.prepare_order_pay_table_min}
                    aria-labelledby="tableTitle"
                    size='medium'
                    aria-label="enhanced table"
                >
                    <EnhancedTableHead headCells={headCells} />
                    <TableBody>
                        {rows.map((row, index) => {
                            return (<Fragment key={index}>
                                <HandlerRowDesktopAndMobile
                                    headCells={headCells}
                                    row={row}
                                    column
                                    index={index}
                                    minimize
                                    className={`${row.orderErrorDates.length ? cls.prepare_order_pay_error_message_rows : row.orderWarningDates.length ? cls.rows_wrapper : ''}`}
                                />
                                {row.orderErrorDates.map((el, index) => (<TableRow key={index} >
                                    <TableCell colSpan={7} >
                                        <div className={cls.prepare_order_pay_error_message}>
                                            <HighlightOffIcon /> {t('general.crossesWithOrder')}{el.orderNum} {t('general.from')} {el.servDateStart} {t('general.by2')} {el.servDateStop} {Boolean(el.flagAuto) ? t('general.repeatAttribute') : ''}
                                        </div>
                                    </TableCell>
                                </TableRow>))}
                                {row.orderWarningDates.map((el, index) => (<TableRow key={index} >
                                    <TableCell colSpan={7}>
                                        <div className={cls.prepare_order_pay_error_message}>
                                            <ErrorIcon style={{ color: '#FEFE00' }} /> {t('general.skipOfDates')} {el.dateStart} {t('general.by2')} {el.dateStop}
                                        </div>
                                    </TableCell>
                                </TableRow>))}
                            </Fragment>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer >
            <div className={cls.block_inform_container_wrapper}>
                <div className={cls.switch_wrapper}>
                    <Switch
                        checked={Boolean(params.flagAuto)}
                        className={cls.switch_style_active}
                        disabled
                    />
                    {t('general.autoRepeat')}
                </div>
                <div className={cls.price_order_wrapper}>{`${t('general.toPay')} ${params.price_order.toFixed(2)} ${t('rub')}`}
                    <HintComponent />
                </div>
            </div>
            <div className={cls.button_block}>
                {
                    !error ? <>
                        <ButtonStyle onClick={resetOrder} styleVariable='outlined'>{t('general.cancel')}</ButtonStyle>
                        <ButtonStyle onClick={() => {
                            if (!warning) {
                                paymentClickHandler();
                            } else {
                                setOpenModalWarning(true);
                            }
                        }}>{t('general.pay')}</ButtonStyle>
                        <Modal open={openModal} close={() => {
                            resetOrder();
                            setOpenModal(false);
                        }}>
                            <ModalContent close={() => {
                                resetOrder();
                                setOpenModal(false);
                            }} />
                        </Modal>
                        <Modal open={openModalWarning} close={() => {
                            setOpenModalWarning(false);
                        }} titleButtom={t('general.pay')} styleButton buttomClick={paymentClickHandler}>
                            <ModalContentWarning />
                        </Modal></>
                        :
                        <ButtonStyle onClick={() => {
                            backStep();
                            toast.error(t('toast.changePeriod'));
                        }} styleVariable='outlined'>{t('general.changePeriod')}</ButtonStyle>
                }</div>
        </Paper >
    );
};

export default PrepareOrderPay;