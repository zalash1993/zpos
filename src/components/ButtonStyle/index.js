import React from 'react';
import PropTypes from 'prop-types';
import cls from './button.module.scss';
import { Button } from '@material-ui/core';

const ButtonStyle = ({children,variant,color,onClick,styleVariable,type,endIcon,disabled}) =>{
    const styleVariant = {
        default:cls.button_default,
        outlined:cls.button_outlined
    }
return(
    <Button disabled={disabled} endIcon={endIcon} type={type} className={disabled ?'':styleVariant[styleVariable]} variant={variant} color={color} onClick={onClick}>
        {children}
    </Button>)
}

ButtonStyle.propTypes = {
    children: PropTypes.any,
    variant: PropTypes.string,
    color: PropTypes.string,
    onClick: PropTypes.func,
    styleVariable: PropTypes.string,
    type: PropTypes.string
};

ButtonStyle.defaultProps = {
    children: '',
    variant: 'contained',
    color: 'default',
    onClick: ()=>{},
    styleVariable: 'default',
    disabled:false
};

export default ButtonStyle;