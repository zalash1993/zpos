import React from 'react';
import cls from './modal.module.scss';
import ButtonStyle from '../ButtonStyle';
import { Dialog } from '@material-ui/core';

const Modal = ({ open, close, children, titleButtom, titleButtom2, buttomClick, styleButton }) => {
    return (<Dialog
        open={open}
        onClose={close}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        className={cls.modal_wrapper}
    >
        <div className={cls.modal_content_wrapper}>
            <div className={cls.close} onClick={close} />
            {children}
            <div className={`${cls.modal_button_block} ${titleButtom ? cls.modal_button_block_two : ''}`}>
                {titleButtom && <ButtonStyle styleVariable={styleButton ? 'default' : 'outlined'} onClick={buttomClick}>
                    {titleButtom}
                </ButtonStyle>}
                {titleButtom2 && <ButtonStyle onClick={close}>
                    {titleButtom2}
                </ButtonStyle>}
            </div>
        </div>
    </Dialog>
    )
}
export default Modal;