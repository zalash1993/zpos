const movementInRef = (ref) => {
    if (ref.current) {
        window.scrollTo({ top: ref.current.offsetTop - 80, behavior: 'smooth' });
    }
};

export default movementInRef;