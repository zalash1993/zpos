const checkToggleStatus = (status) => {
    switch (status) {
        case 'new':
            return false;
        case 'warn':
            return true;
        case 'off':
            return false;
        case 'pre':
            return true;
        case 'ok':
            return true;
        default:
            return false;
    }

};

export default checkToggleStatus;