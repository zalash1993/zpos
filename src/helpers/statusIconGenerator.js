
import React from 'react';
import newImg from '../img/galka.svg';
import preImg from '../img/clock.svg';
import warnImg from '../img/ahtung.svg';
import kursImg from '../img/kurs.svg';
import galkaRed from '../img/galkaRed.svg';

const statusIconGenerator = (status, params, t) => {
    if (params === "doubleIcon") {
        switch (status) {
            // case 'new':
            //     return <div style={{display:'flex', alignItems: 'center',whiteSpace: 'nowrap'}}><img src={newImg} style={{width:20, marginRight:5,height:20}} alt='icon'/> Не активно</div>;
            case 'warn':
                return <div style={{ display: 'flex', alignItems: 'center' }}><img src={warnImg} style={{ width: 20, marginRight: 5, height: 20 }} alt='icon' /> {t('status.expires')}</div>;
            case 'off':
                return <div style={{ display: 'flex', alignItems: 'center' }}><img src={newImg} style={{ width: 20, marginRight: 5, height: 20 }} alt='icon' /> {t('status.executed')}</div>;
            case 'pre':
                return <div style={{ display: 'flex', alignItems: 'center' }}><img src={kursImg} style={{ width: 20, marginRight: 5, height: 20 }} alt='icon' /> {t('status.awaiting')}</div>;
            case 'ok':
                return <div style={{ display: 'flex', alignItems: 'center' }}><img src={preImg} style={{ width: 20, marginRight: 5, height: 20 }} alt='icon' /> {t('status.acting')}</div>;
            default:
                return;
        }
    } else if (params === "icon") {
        switch (status) {
            case 'new':
                return <div style={{ display: 'flex', alignItems: 'center', whiteSpace: 'nowrap' }}><img src={galkaRed} style={{ width: 30, marginRight: 5, height: 30 }} alt='icon' /></div>;
            case 'warn':
                return <div style={{ display: 'flex', alignItems: 'center' }}><img src={warnImg} style={{ width: 30, marginRight: 5, height: 30 }} alt='icon' /></div>;
            case 'pre':
                return <div style={{ display: 'flex', alignItems: 'center' }}><img src={kursImg} style={{ width: 30, marginRight: 5, height: 30 }} alt='icon' /></div>;
            case 'ok':
                return <div style={{ display: 'flex', alignItems: 'center' }}><img src={preImg} style={{ width: 30, marginRight: 5, height: 30 }} alt='icon' /></div>;
            default:
                return;
        }
    } else {
        switch (status) {
            case 'new':
                return <div style={{ display: 'flex', alignItems: 'center', whiteSpace: 'nowrap' }}><img src={galkaRed} style={{ width: 20, marginRight: 5, height: 20 }} alt='icon' /> {t('status.notActive')}</div>;
            case 'warn':
                return <div style={{ display: 'flex', alignItems: 'center' }}><img src={warnImg} style={{ width: 20, marginRight: 5, height: 20 }} alt='icon' /> {t('status.expires')}</div>;
            // case 'off':
            //     return <div style={{display:'flex', alignItems: 'center'}}><img src={newImg} style={{width:20, marginRight:5,height:20}} alt='icon'/> Истекает</div>;
            case 'pre':
                return <div style={{ display: 'flex', alignItems: 'center' }}><img src={kursImg} style={{ width: 20, marginRight: 5, height: 20 }} alt='icon' /> {t('status.awaiting')}</div>;
            case 'ok':
                return <div style={{ display: 'flex', alignItems: 'center' }}><img src={preImg} style={{ width: 20, marginRight: 5, height: 20 }} alt='icon' /> {t('status.acting')}</div>;
            default:
                return;
        }
    }
};

export default statusIconGenerator;