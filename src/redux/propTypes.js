import PropTypes from 'prop-types';

export default {
  default: PropTypes.shape({
    current: PropTypes.string,
  })
};
