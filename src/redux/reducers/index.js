import { combineReducers } from 'redux';
import authentication from './authentication';
import services from './services';
import orders from './orders';
import historyReducer from './historyReducer';
import styleChange from './styleChange';

export default combineReducers({
  authentication,
  services,
  orders,
  historyReducer,
  styleChange
});
