import types from './../Types'

const initialvalues = {
  isRequested: false,
  isRequestedAnOrder: false,
  isRequestedOrderPay: false,
  isRequestedServiceList: false,
  isRequestedServiceDevices: false,
  error: '',
  equipments: [],
  equipmentsLength: 0,
  prepareAnOrder: {},
  prepareOrderPay: {},
  equipmentsActive: [],
  serviceList: [],
  serviceDevices: [],
  orderPay: {}
};

const services = (state = initialvalues, action) => {
  switch (action.type) {
    case types.NEW_SERVICE_LIST_REQUEST:
      return { ...state, isRequestedServiceList: true, error: '' };
    case types.NEW_SERVICE_LIST_SUCCESS:
      return {
        ...state,
        isRequestedServiceList: false,
        serviceList: action.serviceList,
      };
    case types.NEW_SERVICE_LIST_ERROR:
      return {
        ...state,
        isRequestedServiceList: false,
        error: action.error,
      };
    case types.NEW_SERVICE_DEVICES_REQUEST:
      return { ...state, isRequestedServiceDevices: true, error: '' };
    case types.NEW_SERVICE_DEVICES_SUCCESS:
      return {
        ...state,
        isRequestedServiceDevices: false,
        serviceDevices: action.serviceDevices,
      };
    case types.NEW_SERVICE_DEVICES_ERROR:
      return {
        ...state,
        isRequestedServiceDevices: false,
        error: action.error,
      };
    case types.SERVICES_REQUEST:
      return { ...state, isRequested: true, error: '' };
    case types.SERVICES_REQUEST_SUCCESS:
      return {
        ...state,
        isRequested: false,
        equipments: action.equipments,
        equipmentsLength: action.equipmentsLength
      };
    case types.SERVICES_REQUEST_ERROR:
      return {
        ...state,
        isRequested: false,
        error: action.error,
      };
    case types.PREPARE_AN_ORDER_REQUEST:
      return {
        ...state,
        isRequestedAnOrder: true,
        error: ''
      };
    case types.PREPARE_AN_ORDER_SAVE_ORDER_SUCCESS:
      return {
        ...state,
        isRequestedAnOrder: true,
        equipmentsActive: action.equipmentsActive
      };
    case types.PREPARE_AN_ORDER_REQUEST_SUCCESS:
      return {
        ...state,
        isRequestedAnOrder: false,
        prepareAnOrder: action.prepareAnOrder,
      };
    case types.PREPARE_AN_ORDER_REQUEST_ERROR:
      return {
        ...state,
        isRequestedOrderPay: false,
        error: action.error,
      };
    case types.PREPARE_AN_ORDER_PAY_REQUEST:
      return {
        ...state,
        isRequestedOrderPay: true,
        error: ''
      };
    case types.PREPARE_AN_ORDER_PAY_REQUEST_SUCCESS:
      return {
        ...state,
        isRequestedOrderPay: false,
        prepareOrderPay: action.prepareOrderPay,
      };
    case types.PREPARE_AN_ORDER_PAY_REQUEST_ERROR:
      return {
        ...state,
        isRequested: false,
        error: action.error,
      };
    case types.ORDER_PAY_REQUEST:
      return {
        ...state,
        isRequested: true,
        error: ''
      };
    case types.ORDER_PAY_REQUEST_SUCCESS:
      return {
        ...state,
        isRequested: false,
        orderPay: action.orderPay,
      };
    case types.ORDER_PAY_REQUEST_ERROR:
      return {
        ...state,
        isRequested: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export default services;
