import { createSelector } from 'reselect';

const EquipmentState = (state=>state.services.equipments);

const EquipmentSelector = createSelector(EquipmentState,eq=>eq);

export default EquipmentSelector;