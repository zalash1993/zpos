import types from './../Types'
import { toast } from 'react-toastify';

const login = (values, callback) => {
  return (dispatch, getState, { api }) => {
    dispatch({ type: types.USER_REQUEST });
    return api({
      method: 'POST',
      url: `/api/auth/login`,
      data: { login: values.email, password: values.password }
    }).then(
      (data) => {
        const bool = Boolean(data.data.resetPwd);
        localStorage.setItem('token', data.data.token);
        localStorage.setItem('refreshToken', data.data.refreshToken);
        if (!bool) {
          localStorage.setItem('Authorization', true);
          dispatch({ type: types.USER_LOGIN_SUCCESS, user: {}, isLoggedIn: true, resetPassword: bool });
        } else {
          localStorage.removeItem('Authorization');
          dispatch({ type: types.USER_LOGIN_SUCCESS, user: {}, isLoggedIn: false, resetPassword: bool });
        }
        callback && callback();
      },
      ({ response }) => {
        toast.error('Неверный логин или пароль');
        dispatch({ type: types.USER_REQUEST_ERROR, error: response });
        callback && callback(response);
      }
    )
  };
}

const resetPwd = (value, callback) => {
  return (dispatch, getState, { api }) => {
    dispatch({ type: types.USER_REQUEST });
    return api({
      method: 'POST',
      url: '/api/cabinet/resetPassword',
      data: {
        newPassword: value
      },
    }).then(
      (data) => {
        toast.success('Ваш пароль изменен');
        localStorage.setItem('Authorization', false);
        dispatch({ type: types.USER_LOGIN_SUCCESS, user: {}, isLoggedIn: false, resetPassword: false });
        callback && callback();
      },
      ({ response }) => {
        localStorage.removeItem('Authorization');
        dispatch({ type: types.USER_LOGIN_SUCCESS, user: {}, isLoggedIn: false, resetPassword: false });
        dispatch({ type: types.USER_REQUEST_ERROR, error: response })
      },
    );
  };
}

const cardMain = (callback) => {
  return (dispatch, getState, { api }) => {
    dispatch({ type: types.USER_REQUEST });
    // if(!Object.keys(getState().authentication.user).length && localStorage.getItem('user')){
    //   return dispatch({type:types.USER_LOGIN_SUCCESS,user:JSON.parse(localStorage.getItem('user'))});
    // }
    // if(Object.keys(getState().authentication.user).length){
    //   return
    // }
    return api({
      method: 'POST',
      url: '/api/cabinet/my/dashboard'
    }).then(
      (data) => {
        dispatch({ type: types.USER_LOGIN_SUCCESS, isLoggedIn: true, user: data.data });
        // localStorage.setItem('user',JSON.stringify(data.data))
        callback && callback(data);
      },
      ({ response }) => {
        dispatch({ type: types.USER_REQUEST_ERROR, error: response });
        callback && callback(response);
      },
    );
  };
}

const logout = (callback) => {
  return (dispatch, getState, { api }) => {
    dispatch({ type: types.USER_REQUEST });
    const token = localStorage.getItem('token');
    const refreshToken = localStorage.getItem('refreshToken');
    return api({
      method: 'POST',
      url: '/api/auth/exit',
      data: JSON.stringify({
        token,
        refreshToken
      })
    }).then(
      (data) => {
        dispatch({ type: types.USER_IS_NOT_LOGGED_IN });
        localStorage.removeItem('Authorization');
        callback && callback(data);
      },
      ({ response }) => {
        dispatch({ type: types.USER_REQUEST_ERROR, error: response })
        callback && callback(response);
      },
    );
  };
}

const forgotPassword = (login, callback) => {
  return (dispatch, getState, { api }) => {
    dispatch({ type: types.USER_REQUEST });
    return api({
      method: 'POST',
      url: `/api/cabinet/forgotPassword/${login}`
    }).then(
      ({ data: { error } }) => {
        if (error === 0) {
          toast.success('Пароль выслан на e-mail');
          callback && callback();
        } else if (error === 1) {
          toast.error('Не удалось отправить письмо на e-mail');
        }
        dispatch({ type: types.USER_REQUEST_ERROR, error: '' });
      },
      ({ response }) => {
        dispatch({ type: types.USER_REQUEST_ERROR, error: response });
      },
    );
  };
}

export default {
  login,
  logout,
  cardMain,
  resetPwd,
  forgotPassword
};
