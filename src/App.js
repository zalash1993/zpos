import React, { lazy, Suspense, useEffect } from 'react';
import cls from './App.module.scss';
import { Route, Switch, useLocation } from 'react-router-dom';
import Page404 from './pages/Page404';
import 'react-toastify/dist/ReactToastify.css';
// import { useAddToHomescreenPrompt } from './hooks/useAddToHomescreenPrompt';
// import { useTransition, animated } from 'react-spring';
import Footer from './pages/Footer';
import Header from './pages/Header';
import InformationUser from './pages/InformationUser';
import Contacts from './pages/Contacts';
import FAQ from './pages/FAQ';
import Skeleton from '@material-ui/lab/Skeleton';
// import InformBlock from './components/InformBlock';
// import Modal from './components/Modal';
// import { Container } from '@material-ui/core';
// import Settings from './pages/Settings';
// const Equipment = lazy(() => import('./pages/Equipment'));
import Equipment from './pages/Equipment';
import Orders from './pages/Orders';
// const Orders = lazy(() => import('./pages/Orders'));
const AddService = lazy(() => import('./pages/AddService'));
const HistoryOrders = lazy(() => import('./pages/HistoryOrders'));


const App = () => {
  useEffect(()=>{
    // debugger
    // window.scrollTo({ top: 0, behavior: 'smooth' });
  },[])
  const location = useLocation();
  // const [isVisible, setVisibleState] = useState(false);
  // const [prompt, promptToInstall] = useAddToHomescreenPrompt();
  // useEffect(
  //   () => {
  //     if (prompt) {
  //       setVisibleState(true);
  //     }
  //   },
  //   [prompt]
  // );
  // const transitions = useTransition(location, location => location.pathname, {
  //   from: {
  //     opacity: 0,
  //     position: 'absolute',
  //     width: '90%',
  //     left: '50vw'
  //   },
  //   enter: {
  //     position: 'inherit',
  //     opacity: 1,
  //     width: '100%',
  //     left: '0vw'
  //   },
  //   leave: {
  //     position: 'absolute',
  //     opacity: 0,
  //     width: '90%',
  //     left: "-50vw"
  //   },
  //   unique: false,
  //   reset: true
  // });
  return (
    <div className={`${cls.App} ${location.pathname === "/" ? cls.App_overflow : ''}`}>
      {/* <Modal open={isVisible} close={() => setVisibleState(false)} titleButtom='закрыть' titleButtom2='добавить' buttomClick={() => {
        promptToInstall()
        setVisibleState(false)
      }}>
        Здравствуйте! Хотите добавить приложение на домашний экран?
      </Modal> */}
      <Header />
      <div className={cls.home_content}>
        {/* <Container style={{ position: 'relative' }} component="main" maxWidth="lg"> */}
          {/* <InformBlock /> */}
          <Suspense fallback={<Skeleton animation="wave" variant="rect" style={{ marginTop: 20, height: '400px !important' }} />}>
            {/* {transitions.map(({ item, props: transition, key }) => (
              <animated.div
                id='animated'
                // className={newClass} 
                key={key}
                style={transition}
              > */}
                <Switch 
                // location={item}
                >
                  <Route exact path='/' component={InformationUser} />
                  <Route exact path='/equipment' component={Equipment} />
                  <Route exact path='/orders' component={Orders} />
                  <Route exact path='/FAQ' component={FAQ} />
                  <Route exact path='/сontacts' component={Contacts} />
                  <Route exact path='/history' component={HistoryOrders} />
                  {/* <Route exact path='/settings' component={Settings} /> */}
                  <Route exact path='/add-service' component={AddService} />
                  <Route component={Page404} />
                </Switch>
              {/* </animated.div>))} */}
          </Suspense>
        {/* </Container> */}
        <Footer />
      </div>
    </div>
  );
}

export default App;
